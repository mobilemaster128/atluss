let mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/assets/js/app.js", "public/js")
  .js("resources/assets/js/admin.js", "public/js")
  .scripts([
    'resources/assets/bower/jquery-ui/jquery-ui.js',
    'resources/assets/bower/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
    'resources/assets/bower/moment/moment.js',
    'resources/assets/bower/moment-timezone/builds/moment-timezone-with-data.js',
    'resources/assets/bower/fullcalendar/dist/fullcalendar.js',
    // 'resources/assets/bower/inputmask/dist/jquery.inputmask.bundle.js',
    // 'resources/assets/bower/card-js/card-js.min.js'
  ], 'public/js/bower.js')
  .scripts([
    'resources/assets/bower/moment/moment.js',
    'resources/assets/bower/moment-timezone/builds/moment-timezone-with-data.js',
    'resources/assets/bower/fullcalendar/dist/fullcalendar.js'
  ], 'public/js/admin-bower.js')
  .styles([
    'resources/assets/bower/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
    'resources/assets/bower/jquery-ui/themes/base/jquery-ui.css',
    'resources/assets/bower/fullcalendar/dist/fullcalendar.css',
    // 'resources/assets/bower/card-js/card-js.min.css'
  ], 'public/css/bower.css')
  .styles([
  ], 'public/css/admin-bower.css')
  .copyDirectory("resources/assets/img", "public/img")
  .copyDirectory("resources/assets/bower/jquery-ui/themes/base/images", "public/css/images")
  .sass("resources/assets/sass/app.scss", "public/css/app.css")
  .sass("resources/assets/sass/admin.scss", "public/css/admin.css")
  .less("resources/assets/less/app.less", "public/css/app-style.css")
  .less("resources/assets/less/admin.less", "public/css/admin-style.css");
