<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AppointmentEmail extends Notification implements ShouldQueue
{
    use Queueable;

    public $appointment;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {  
        $types = config('constant.type_meeting');
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->appointment->start);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $this->appointment->end);
        $stock = (empty($this->appointment->advisor->emailSetting) || empty($this->appointment->advisor->emailSetting->appointment_content)) ? 
                    'New Appointment Created' : $this->appointment->advisor->emailSetting->appointment_content;
        if ($this->appointment->advisor_id == $notifiable->id) {
            $action = $this->appointment->advisor->name.'\'s Schedule';
            $url = url("schedule/".$start->format('U'));
        } elseif ($this->appointment->client_id == $notifiable->id) {
            $action = $this->appointment->advisor->name.'\'s Calendar';
            $url = url("calendar/".$this->appointment->advisor->id);
        } else {
            $action = $this->appointment->advisor->name.'\'s Calendar';
            $url = url("calendar/".$this->appointment->advisor->id);
        }                   
        // $agency_name = $this->appointment->advisor->clients()->find($this->appointment->client_id)->pivot->agency->name;
        $agency_name = $this->appointment->agency;

        $mailMessage = (new MailMessage)
                    ->subject('New Financial Appointment for ' . $this->appointment->advisor->name)
                    // ->greeting('Hello')
                    ->line($stock)
                    ->action($action, $url)
                    ->line('Appointment Time: '.$start->format('n/d/Y h:i:s A').' ~ '.$end->format('n/d/Y h:i:s A'))
                    ->line('Agency Name: '.$agency_name)
                    ->line('Appointment Type: '.$types[$this->appointment->type])
                    ->line('Appointment Set By: '.$this->appointment->setby);
        if ($this->appointment->advisor_id == $notifiable->id || $this->appointment->client_id == $notifiable->id) {
            $mailMessage = $mailMessage->line('Contact Name: '.$this->appointment->name)
                        ->line('Contact Address: '.$this->appointment->address)
                        ->line('Contact City: '.$this->appointment->city)
                        ->line('Zip Code: '.$this->appointment->zip_code)
                        ->line('State: '.$this->appointment->state)
                        ->line('Contact Phone Number: '.$this->appointment->phone_number)
                        ->line('Contact Email: '.$this->appointment->email)
                        ->line('Reason for Appointment/Details: '.$this->appointment->description);
        }
        $mailMessage = $mailMessage->line('Thank you for using our application!');

        return $mailMessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
