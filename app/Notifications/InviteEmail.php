<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InviteEmail extends Notification implements ShouldQueue
{
    use Queueable;

    public $invite;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($invite)
    {
        $this->invite = $invite;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url("invite/accept/".$this->invite->id."/".$this->invite->token);
        return (new MailMessage)
                    ->subject('User Invitation')
                    ->line($this->invite->user->name.' has invited you to use their Virtual Connection Toolkit, Atluss.')
                    ->line('Please click HERE, and follow the instructions to login.')
                    ->action('Accept Invitation',  $url)
                    ->line('Once you’re registered you’ll find that this tool is exceedingly powerful, yet easy to use and will boost the number of financial appointments you are setting!')                    
                    ->line('Happy Setting!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
