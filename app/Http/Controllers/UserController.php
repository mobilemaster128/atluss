<?php

namespace App\Http\Controllers;

use App\User;
use App\Timeslot;
use App\State;
use Carbon\Carbon;
use App\Mail\MessageMail;
use Illuminate\Http\Request;
use App\Notifications\DailyEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;
use App\Mail\AdminMail;

use function GuzzleHttp\Promise\all;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Auth::user()->profile;
        if ($profile !== NULL) {
            if (Auth::user()->isAdvisor()) {
                return view('dashboard');
            } else {
                $users = Auth::user()->advisors;
                return view('client.dashboard', ['users' => $users]);
            }
        }
        if (Auth::user()->isClient()) {
            $users = Auth::user()->advisors;
            return view('client.dashboard', ['users' => $users]);
        }
        return redirect('subscribe');
//        return view('subscription');
    }

    public function profile($monthly)
    {
        $states = State::all();

        $profile = Auth::user()->profile;
        if ($profile !== NULL) {
            $profile->expires_at = date('m/d/Y', strtotime($profile->expires_at));
        }

        return view('auth.profile', ['profile' => $profile, 'states' => $states, 'monthly' => $monthly, 'err' => '']);
    }

    public function setProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'card_name' => 'required|string|min:3|max:64',
            'card_number' => 'required|string|min:3|max:64',
            'expires_at' => 'required|string',
            'cvc' => 'required|string|min:3|max:64',
            'user_name' => 'required|string|min:3|max:64',
            'address' => 'required|string|min:3|max:255',
            'city' => 'required|string|min:3|max:64',
            'state' => 'required|integer|exists:states,id',
            'zip_code' => 'required|string|min:3|max:64',
            'timezone' => 'nullable|integer',
        ]);
        $validator->validate();

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $monthly = $request['monthly'] == 1 ? "month" : "year";
        $name = $request['monthly'] == 1 ? "Monthly" : "Yearly";
        $value = $request['monthly'] == 1 ? 5000 : 55000;
        try {
            $product = \Stripe\Product::retrieve($name);
        } catch (\Stripe\Error\InvalidRequest $e ) {
            $product = \Stripe\Product::create([
                'id' => $name,
                'name' => $name,
                'type' => 'service',
            ]);
        }

        try {
            $plan = \Stripe\Plan::retrieve($name);
        } catch (\Stripe\Error\InvalidRequest $e) {
            $plan = \Stripe\Plan::create([
                'id' => $name,
                'currency' => 'usd',
                'interval' => $monthly,
                'product' => $product,
                'nickname' => $name,
                'amount' => $value,
            ]);
        }

        try {
            Auth::user()->newSubscription('main', $name)->create($request['stripeToken']);
            // Auth::user()->newSubscription('main', $name)->trialDays(30)->create($request['stripeToken']);
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error']['message'];
            $states = State::all();
            $profile = $request;
            return view('auth.profile', ['profile' => $profile, 'states' => $states, 'monthly' => $request['monthly'], 'err' => $err]);
        }

        Auth::user()->profile()->updateOrCreate(
            [],
            [
                'card_number' => $request['card_number'],
                'card_name' => $request['card_name'],
                'expires_at' => Carbon::createFromFormat('m/y', $request->expires_at),
                'cvc' => $request['cvc'],
                'user_name' => $request['user_name'],
                'address' => $request['address'],
                'city' => $request['city'],
                'state' => State::find($request['state'])->name,
                'zip_code' => $request['zip_code'],
            ]
        );

        Auth::user()->emailSetting()->updateOrCreate(
            [],
            [
                'daily_content' => 'This is your Financial Specialist - ' . Auth::user()->name . ' daily schedule... Click on my schedule link to view availability. Daily Quote:"Alone we can do so little, together we can do so much." ~ Helen Keller',
                'daily_time' => Carbon::createFromFormat('H:i:s', '08:00:00'),
                'appointment_content' => 'A new financial appointment has just been set; check out details below... Thank you for taking the first step in protecting this valued customer\'s financial future.',
                'prior_time' => 24,
                'reminder_content' => 'Just a friendly reminder for you to confirm ' . Auth::user()->name . ' appointment that you set for tomorrow. [Appointment details can be found below]',
                'timezone' => $request['timezone'],
            ]
        );
        $user = Auth::user();        

        Mail::to(['lukas.gil2020@gmail.com', 'klinton@bluetonemedia.com', 'wherring@atluss.com'])
            ->send(new AdminMail($user->name, $user->email, $user->created_at));

        return redirect(route('dashboard'));
    }

    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'card_name' => 'required|string|min:3|max:64',
            'card_number' => 'required|string|min:3|max:64',
            'expires_at' => 'required|string',
            // 'expire_month' => 'required|integer|min:1|max:12',
            // 'expire_year' => 'required|integer|min:1|max:99',
            'cvc' => 'required|string|min:3|max:64',
            'user_name' => 'required|string|min:3|max:64',
            'address' => 'required|string|min:3|max:255',
            'city' => 'required|string|min:3|max:64',
            'state' => 'required|integer|exists:states,id',
            'zip_code' => 'required|string|min:3|max:64',
        ]);
        $validator->validate();
        try {
            Auth::user()->updateCard($request['stripeToken']);
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error']['message'];
            return back()->with('error', $err);
        }

        Auth::user()->profile()->updateOrCreate(
            [], 
            [
                'card_number' => $request['card_number'],
                'card_name' => $request['card_name'],
                // 'expires_at' => Carbon::createFromFormat('m/d/Y', $request->expire_month.'/01/20'.$request->expire_year),
                'expires_at' => Carbon::createFromFormat('m/y', $request->expires_at),
                'cvc' => $request['cvc'],
                'user_name' => $request['user_name'],
                'address' => $request['address'],
                'city' => $request['city'],
                'state' => State::find($request['state'])->name,
                'zip_code' => $request['zip_code'],
            ]
        );

        return back()->with('status', 'Updated payment information!');
    }

    public function cancelProfile(Request $request)
    {
        Auth::user()->subscription('main')->cancelNow();
        Auth::user()->profile()->delete();
        return redirect(route('dashboard'));
    }

    public function updateInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'company' => 'required|string|max:255',
        ]);
        $validator->validate();

        $user = Auth::user();
        $user->name = $request['name'];
        $user->company = $request['company'];
        $user->save();        

        return back()->with('status', 'Updated user information!');
    }
    
    public function userlist()
    {
        $user = Auth::user();
        $users = $user->clients;
        $invites = $user->invites()->where('expires_at', '>', now())->get();
        $agencies = Auth::user()->agencies;
        
        return view('advisor.userlist', ['users' => $users, 'invites' => $invites, 'agencies' => $agencies]);
    }
    
    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:users',
            'agency' => 'required|string|exists:agencies,id',
        ]);
        $validator->validate();

        $user = Auth::user();
        $client = $user->clients()->find($request->id);

        $agency = $user->agencies()->find($request->agency);
        if ($agency === NULL) {
            throw ValidationException::withMessages([
                'agency' => ['Invalid Agency'],
            ]);
        }

        if ($client !== NULL) {
            $user->clients()->updateExistingPivot($client,  ['agency_id' => $agency->id]);
        }

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }
        
        return back()->with('success', 'Updated user information!');
    }
    
    public function removeUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:users',
        ]);
        $validator->validate();

        $user = Auth::user();
        $client = $user->clients()->find($request->id);

        if ($client !== NULL) {
            $user->clients()->detach($client);
        }

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }
    
        return back()->with('success', 'Removed');
    }
    
    public function account()
    {
        $user = Auth::user();
        $profile = $user->profile;
        if ($profile !== NULL) {
            $profile->expires_at = Carbon::createFromFormat('Y-m-d', $profile->expires_at)->format('m/y');
            // $profile->expiry_value = date('m', strtotime($profile->expires_at)).'/'.date('y', strtotime($profile->expires_at));
        }
        $states = State::all();
        
        return view('account', [
            'user' => $user,
            'profile' => $profile,
            'states' => $states,
        ]);
    }

    public function help()
    {
        return view('help');
    }
    
    public function feedback()
    {
        return view('feedback');
    }
    
    public function setting()
    {
        return view('advisor.setting');
    }
    
    public function emailSetting()
    {
        $min_hour = 24;
        $emailSetting = Auth::user()->emailSetting;
        $daily_time = ($emailSetting != NULL && $emailSetting->daily_time!= NULL) ? 
                        Carbon::createFromFormat('H:i:s', $emailSetting->daily_time) : Carbon::createFromFormat('H:i:s', '08:00:00');
        $timezone = $emailSetting != NULL ? $emailSetting->timezone * -1 : 0;
        $daily_time->add(\DateInterval::createfromdatestring("$timezone minutes"));
        $daily_content = ($emailSetting && $emailSetting->daily_content) ? $emailSetting->daily_content : '';
        $appointment_content = ($emailSetting && $emailSetting->appointment_content) ? $emailSetting->appointment_content : '';
        $reminder_content = ($emailSetting && $emailSetting->reminder_content) ? $emailSetting->reminder_content : '';
        $prior_time = ($emailSetting && intval($emailSetting->prior_time) > $min_hour) ? $emailSetting->prior_time : $min_hour;

        return view('advisor.emailsetting', [
            'daily_content' => $daily_content,
            'daily_time' => $daily_time->format('h:i A'),
            'appointment_content' => $appointment_content,
            'reminder_content' => $reminder_content,
            'prior_time' => $prior_time,
        ]);
    }

    public function updateEmailSetting(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'daily_content' => 'nullable|string',
            'daily_time' => 'nullable|string',
            'appointment_content' => 'nullable|string',
            'reminder_content' => 'nullable|string',
            'prior_time' => 'nullable|integer',
            'timezone' => 'nullable|integer',
            'send_now' => 'boolean',
        ]);
        $validator->validate();

        $min_hour = 24;
        if ($request->daily_time != NULL) {
            $daily_time = Carbon::createFromFormat('h:i A', $request->daily_time);
            if ($request->timezone != NULL) {
                $daily_time->add(\DateInterval::createfromdatestring("$request->timezone minutes"));
            }
            $daily_time = $daily_time;
        }
        $prior_time = intval($request->prior_time) > $min_hour ? $request->prior_time : $min_hour;
        Auth::user()->emailSetting()->updateOrCreate(
            [
            ],
            [
                'daily_content' => $request->daily_content,
                'daily_time' => $daily_time,
                'appointment_content' => $request->appointment_content,
                'prior_time' => $prior_time,
                'reminder_content' => $request->reminder_content,
                'timezone' => $request->timezone,
            ]
        );
        
        if ($request->send_now === TRUE) {
            $receivers = [];
            foreach(Auth::user()->clients as $client) {
                array_push($receivers, $client);
            }
            if (count($receivers) > 0) {
                Notification::send($receivers, new DailyEmail(Auth::user()));
            }
            Notification::send(Auth::user(), new DailyEmail(Auth::user()));
        }

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }

        return back()->with('success', 'Updated user information!');
    }

    public function calendarSetting()
    {
        $calendarSetting = Auth::user()->calendarSetting;
        $calendar_setting = ($calendarSetting && $calendarSetting->calendar_setting) ? $calendarSetting->calendar_setting : 4;

        return view('advisor.calendarsetting', [
            'calendar_setting' => $calendar_setting,
        ]);
    }

    public function updateCalendarSetting(Request $request)
    {
        Auth::user()->calendarSetting()->updateOrCreate(
            [
            ],
            [
                'calendar_setting' => $request->calendar,
            ]
        );

        return back()->with('success', 'Updated user information!');
    }

    public function sendMessage(Request $request)
    {                
        $validator = Validator::make($request->all(), [            
            'message' => 'required|string|min:6',
            'subject' => 'required|string|min:3|max:64'
        ]);
        $validator->validate();            
        $user = Auth::user();
        if ($user->isAdvisor()) {
            $users = $user->clients;
        } elseif ($user->isClient()) {
            $users = $user->advisors;
        }   
                
        $link = url("calendar/".$user->id);                
        // Mail::to(['lukas.gil2020@gmail.com', 'klinton@bluetonemedia.com'])
        //     ->send(new MessageMail($request->message, $request->subject, $link,$user->name ));        
        Mail::to($user->email)
            ->send(new MessageMail($request->message, $request->subject, $link,$user->name ));        
        foreach($users as $user) {            
            Mail::to($user->email)
            ->send(new MessageMail($request->message, $request->subject, $link, Auth::user()->name));
        }
        
        return back()->with('status', 'Thanks for Message!');
    }
    
    public function statistic($user_id = NULL)
    {     
        if ($user_id === NULL) {
            $user = Auth::user();
        } else {
            if (Auth::user()->isAdvisor()) {
                $user = Auth::user()->clients()->find($user_id);
            } elseif (Auth::user()->isClient()) {
                $user = Auth::user()->advisors()->find($user_id);
            } 
            if ($user == NULL) {
                throw ValidationException::withMessages([
                    'user' => ['Invalid User'],
                ]);
            }
        }
        if ($user->isAdvisor()) {
            $users = $user->clients;
        } elseif ($user->isClient()) {
            $users = $user->advisors;
        } 
        return view('statistic', [
            'users' => $users,
            'user' => $user_id ? $user : NULL,
        ]);
    }
    
    public function getStatistic(Request $request, $user_id = NULL) {
        $validator = Validator::make($request->all(), [
            'start' => 'required|integer',
            'end' => 'required|integer',
        ]);
        $validator->validate();
               
        if ($user_id === NULL) {
            $user = Auth::user();
        } else {
            if (Auth::user()->isAdvisor()) {
                $user = Auth::user()->clients()->find($user_id);
            } elseif (Auth::user()->isClient()) {
                $user = Auth::user()->advisors()->find($user_id);
            } 
            if ($user == NULL) {
                throw ValidationException::withMessages([
                    'user' => ['Invalid User'],
                ]);
            }
        }
        $start = Carbon::createFromFormat('U', $request->start);
        $end = Carbon::createFromFormat('U', $request->end);
        
        if ($user->isAdvisor()) {
            $result = $user->clients;
            foreach ($result as $value) {
                $value->load(['appointments' => function($query) use ($start, $end) {
                    $query->where('start', '<', $end)->where('end', '>', $start)->orderBy('start')->orderBy('end')->get();
                }]);
            }
        } elseif ($user->isClient()) {
            $result = $user->advisors;
            foreach ($result as $value) {
                $value->load(['appointments' => function($query) use ($start, $end) {
                    $query->where('start', '<', $end)->where('end', '>', $start)->orderBy('start')->orderBy('end')->get();
                }]);
            }
        }
        
        return response()->json(['success' => true, 'statistics' => $result]);
    }

    public function subscription()
    {
//        return redirect('subscribe');
        return view('subscription');
    }

    public function agencySetting()
    {
        $agencies = Auth::user()->agencies()->withCount('advisor_client_group')->withCount(['invites' => function($query) {
            $query->where('expires_at', '>', now());
        }])->with('advisor_client_group.client')->with(['invites' => function($query) {
            $query->where('expires_at', '>', now());
        }])->get();

        $clients = Auth::user()->clients;

        return view('advisor.agencysetting', ['agencies' => $agencies, 'clients' => $clients]);
    }

    public function createAgency(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'agency' => 'required|string|min:2|max:255',
        ]);
        $validator->validate();

        Auth::user()->agencies()->updateOrCreate(['name' => $request->agency], []);

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }
    
        return back()->with('success', 'Created');
    }

    public function updateAgency(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:agencies',
            'name' => 'required|string|min:2|max:255',
        ]);
        $validator->validate();

        $user = Auth::user();
        $agency = $user->agencies()->find($request->id);

        if ($agency == NULL) {
            throw ValidationException::withMessages([
                'id' => ['Invalid Agency'],
            ]);
        }
        if ($user->agencies()->where('id', '<>', $agency->id)->where('name', $request->name)->first() !== NULL) {
            throw ValidationException::withMessages([
                'name' => ['Duplicated Agency Name'],
            ]);
        }
        $agency->name = $request->name;
        $agency->save();

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }
    
        return back()->with('success', 'Removed');
    }   
    
    public function editAgency(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:agencies',
            'clients' => 'array',
        ]);
        $validator->validate();

        $user = Auth::user();
        $agency = $user->agencies()->find($request->id);

        if ($agency !== NULL && $request->clients != NULL) {
            foreach ($request->clients as $value) {
                $client = $user->clients()->find($value);
                if ($client !== NULL) {
                    $user->clients()->updateExistingPivot($client,  ['agency_id' => $agency->id]);
                }
            }
        }

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }
    
        return back()->with('success', 'Removed');
    }  

    public function removeAgency(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:agencies',
        ]);
        $validator->validate();

        $user = Auth::user();
        $agency = $user->agencies()->find($request->id);

        if ($agency !== NULL) {
            $agency->delete();
        }

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }
    
        return back()->with('success', 'Removed');
    }   
}
