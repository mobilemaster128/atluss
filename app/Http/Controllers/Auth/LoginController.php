<?php

namespace App\Http\Controllers\Auth;

use App\Invite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Bestmomo\LaravelEmailConfirmation\Traits\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard';

    protected $acceptURL = NULL;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function redirectTo() {
        $user = $this->guard()->user();
        if ($user->isAdvisor()) {
            if ($user->profile == NULL) {
                return 'subscription';
            }
        } elseif ($user->isAdmin()) {
            return 'admin/dashboard';
        } elseif ($user->isClient()) {
            return $this->acceptURL ?: 'dashboard';
        }
        return $this->redirectTo;
    }

    protected function login(Request $request) {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (!$this->checkCredential($request)) {
            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }
        
        $user = $this->guard()->getLastAttempted();
        
        if ($user->confirmed) {
            // If user is confirmed we make the login and delete session information if needed
            $this->attemptLogin($request);
            if ($request->session()->has('user_id')) {
                $request->session()->forget('user_id');
            }
            $emailSetting = $this->guard()->user()->emailSetting;
            if ($emailSetting) {
                if ($emailSetting->timezone != $request['timezone']) {
                    $emailSetting->timezone = $request['timezone'];
                    $emailSetting->save();
                }
            }
            return $this->sendLoginResponse($request);
        }

        $request->session()->put('user_id', $user->id);

        return back()->with('confirmation-danger', __('confirmation::confirmation.again'));
    }

    public function accept(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'invite_id' => 'required|integer|exists:invites,id',
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
        $validator->validate();

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        
        if (!$this->checkCredential($request)) {
            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }
        
        $user = $this->guard()->getLastAttempted();
        if (!$user->isClient()) {
            $this->guard()->logout();
            return back()->with('confirmation-danger', trans('invite.invalid_invite'));
        }
        $user->confirmed = true;
        $user->save();

        $this->attemptLogin($request);

        $invite = Invite::find($request->invite_id);
        $invite->delete();

        $user = $this->guard()->user();

        $user->advisors()->attach($invite->user, ['agency_id' => $invite->agency_id]);

        if ($request->session()->has('user_id')) {
            $request->session()->forget('user_id');
        }
        
        $this->acceptURL = "client/calendar/$invite->user_id";
        return $this->sendLoginResponse($request);
    }
}
