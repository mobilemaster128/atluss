<?php

namespace App\Http\Controllers;

use App\User;
use App\Invite;
use Carbon\Carbon;
use App\Notifications\InviteEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }
    
    public function index()
    {
        return view('admin.home');
    }
    
    public function users()
    {
        $users = User::latest()->where('type', 2)->with('profile')->paginate();
        
        foreach ($users as $user) {
            $user->load('clients');
            $user->clients = $user->clients->count();
        }
        
        return view('admin/users', [
            'users' => $users,
        ]);
    }
    
    public function verify_email($id, $verify)
    {
        $users = User::where('id', $id)->firstOrFail();
        $users->confirmed = $verify;
        $users->save();

        return redirect('/admin/users');
    }

    public function delete_user($id)
    {
        $users = User::with('profile', 'subscriptions')->findOrFail($id);
        $profiles = $users->profile;
        $subscriptions = $users->subscriptions;

        if($subscriptions)
        foreach($subscriptions as $subscription)
            $subscription->delete();

        if($profiles) $profiles->delete();
        if($users)
            $users->delete();

        return redirect('/admin/users');
    }

    public function invites() {
        $invites = Invite::latest()->with('user:id,name,type')->paginate();
        
        return view('admin/invites', [
            'invites' => $invites,
        ]);
    }
    
    public function showInviteForm() {
        return view('admin/invite');
    }
    
    public function invite(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            // 'g-recaptcha-response' => 'required|recaptcha',
        ]);
        $validator->validate();

        $invite = Auth::user()->invites()->updateOrCreate(
            [
                'email' => $request['email'],
            ],
            [
                'token' => $this->generateToken(),
                'expires_at' => now(config('app.timezone'))->addDays(30),
            ]
        );

        Notification::route('mail', $invite->email)
                    ->notify(new InviteEmail($invite));

        return back()->with('success', trans('invite.success'));
    }
    
    protected function generateToken() {
        $token = md5(uniqid());
        if (Invite::where('token', $token)->first()) {
            return $this->generateToken();
        }
        return $token;
    }
}
