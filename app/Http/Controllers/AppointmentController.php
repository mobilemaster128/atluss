<?php

namespace App\Http\Controllers;

use App\User;
use App\State;
use App\Timeslot;
use App\Appointment;
use Carbon\Carbon;
use App\Notifications\AppointmentEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;

class AppointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id = NULL)
    {
        if ($user_id != NULL) {
            $advisor = Auth::user()->advisors()->find($user_id);
            if ($advisor === NULL) {
                return abort('400', 'Invalid Advisor');
            }
        }
        $calendarSetting = Auth::user()->calendarSetting;
        $calendar_setting = ($calendarSetting && $calendarSetting->calendar_setting) ? $calendarSetting->calendar_setting : 4;

        return view('advisor.calendar', [
            'user' => $user_id ? $advisor : NULL,
            'calendar_setting' => $calendar_setting,
        ]);
    }
    
    public function bookingCalendar(Request $request, $user_id, $timeStamp = NULL)
    {
        $advisor = Auth::user()->advisors()->find($user_id);
        if ($advisor === NULL) {
            return abort('400', 'Invalid Advisor');
        }

        $lengths = config('constant.length_meeting');
        $types = config('constant.type_meeting'); 
        $states = State::all();
           
        if ($timeStamp === NULL) {
            $timeStamp = time();
        }
        
        $start_day = Carbon::createFromFormat('U', $timeStamp)->setTime(0, 0, 0);
        $end_day = Carbon::createFromFormat('U', $timeStamp)->setTime(23, 59, 59);
        
        $appointments = $advisor->appointments()->where('start', '<', $end_day)->where('end', '>', $start_day)->orderBy('start')->orderBy('end')->get();
        
        foreach ($appointments as $value) {
            $value->start = Carbon::createFromFormat('Y-m-d H:i:s', $value->start);
            $value->end = Carbon::createFromFormat('Y-m-d  H:i:s', $value->end);
        }
        
        $appointments = $this->arrangeAppointments($start_day, $appointments);

//        $weekday = Carbon::createFromFormat('U', $timeStamp)->format('w');
        $weekday = date('w', $timeStamp);
        $timeslots = $advisor->timeslots()->where('weekday', $weekday)->orderBy('start_time', 'asc')->get();
        $result = $this->arrangeTimeslot($start_day, $timeslots, $appointments);
        
        return view('client.calendar', [
            'start_day' => $start_day,
            'appointments' => $appointments->merge($result)->sortBy('start'),
            'timeStamp' => $timeStamp, 
            'advisor' => $advisor, 
            'lengths' => $lengths, 
            'types' => $types,
            'states' => $states,
            'mode' => $request->query('mode') ?: 'year'
            // 'mode' => $request->mode ?: 'month'
        ]);
    }   
    
    public function search(Request $request, $user_id = NULL)
    {
        $validator = Validator::make($request->all(), [
            'start' => 'required|integer',
            'end' => 'required|integer',
            // 'timezone' => 'required|timezone',
        ]);
        $validator->validate();
        
        $user = Auth::user();
        if ($user_id != NULL && $user->isClient() == true) {
            $advisor = $user->advisors()->find($user_id);
            if ($advisor === NULL) {
                throw ValidationException::withMessages([
                    'user' => [trans('error.invalid_advisor')],
                ]);
            }
        }

        $start = Carbon::createFromFormat('U', $request->start);
        $end = Carbon::createFromFormat('U', $request->end);
        
        $result = $this->searchAppointment($start, $end, isset($advisor) ? $advisor : NULL);  
        
        return response()->json(['success' => true, 'appointments' => $result]);
    }
    
    protected function searchAppointment($start, $end, $advisor = NULL) {  
        $colors = config('constant.color_meeting');      
        if ($advisor !== NULL) {
            $appointments = $advisor->appointments()->where('start', '<', $end)->where('end', '>', $start)->orderBy('start')->orderBy('end')->get();
        } else {
            $appointments = Auth::user()->appointments()->where('start', '<', $end)->where('end', '>', $start)->orderBy('start')->orderBy('end')->get();
        }        

        foreach ($appointments as $appointment) {
            $appointment->start = Carbon::createFromFormat('Y-m-d H:i:s', $appointment->start);
            $appointment->end = Carbon::createFromFormat('Y-m-d  H:i:s', $appointment->end);
            $appointment->textColor = $appointment->status == true ? 
                    ($appointment->type == 0 ? $colors[0] : ($appointment->type == 1 ? $colors[3] : 
                        ($appointment->type == 10 ? $colors[2] : $colors[1]))) : $colors[1];
            $appointment->backgroundColor = '#fff';
            $appointment->borderColor = '#fff';
            $appointment->title = $appointment->title;
            if ($advisor !== NULL) {
                $appointment->editable = ($appointment->client_id == Auth::user()->id) || 
                                            ($advisor->clients()->find($appointment->client_id) != NULL && 
                                                $advisor->clients()->find($appointment->client_id)->pivot->agency_id == $advisor->pivot->agency_id) ? true : false;
                $appointment->startEditable = ($appointment->client_id == Auth::user()->id) || 
                                                ($advisor->clients()->find($appointment->client_id) != NULL && 
                                                    $advisor->clients()->find($appointment->client_id)->pivot->agency_id == $advisor->pivot->agency_id) ? true : false;
                if ($appointment->type < 10) {
                    $appointment->className = ($appointment->client_id == Auth::user()->id ? 'pointer ' : '').($appointment->type == 1 ? 'phone' : 'office');
                }
            } else {
                $appointment->editable = true;
                $appointment->startEditable = true;
                $appointment->className = $appointment->type == 1 ? 'pointer phone' : 'pointer office';
            }
            $appointment->durationEditable = false;
        }
        
        $appointments = $this->arrangeAppointments($start, $appointments);

        $result = [];//$appointments->toArray();    

        if ($advisor !== NULL) {
            $timeslots = $advisor->timeslots()->orderBy('start_time', 'asc')->get();
        } else {
            $timeslots = Auth::user()->timeslots()->orderBy('start_time', 'asc')->get();
        }
        $timeslots = $timeslots->groupBy('weekday')->toArray();
        
        for ($date = clone $start; $date <= $end; $date->add(new \DateInterval('P1D'))) {
            $weekday = intval($date->format('w'));
            if (isset($timeslots[$weekday])) {
                $data = $this->arrangeTimeslot($date, $timeslots[$weekday], $appointments);
                foreach ($data as $value) {
                    $value->start = $value->start->format('Y-m-d H:i:s');
                    $value->end =  $value->end->format('Y-m-d H:i:s');
                    array_push($result, $value);
                }
            }
        }
        
        foreach ($appointments as $appointment) {
            $appointment->start = $appointment->start->format('Y-m-d H:i:s');
            $appointment->end = $appointment->end->format('Y-m-d H:i:s');
            array_push($result, $appointment);
        }
        
        return $result;
    }

    protected function arrangeAppointments($date, $appointments) {
        $colors = config('constant.color_meeting');
        $result = [];
        $year = intval($date->format('Y'));
        $month = intval($date->format('n'));
        $day = intval($date->format('j'));
        foreach($appointments as $appointment) {
            if ($appointment->type < 10) {
                array_push($result, $appointment);
                continue;
            }
            $start_date = $appointment->start;
            $end_date = $appointment->end;

            $filtered = $appointments->filter(function($value, $key) use ($start_date, $end_date, $appointment) {
                if ($appointment['type'] == 10) {
                    return ($value->client_id != NULL && $value->start < $end_date && $value->end > $start_date);
                } else {
                    return ($value->client_id != NULL && $value->start < $end_date && $value->end > $start_date);// && $value->type == 10);
                }
            })->all();

            // if (count($filtered) > 0) {
            //     continue;
            // } else {
            //     array_push($result, $appointment);
            //     continue;
            // }

            $aps_date = [];

            if (count($filtered) > 0) {
                $ap_start = $start_date;
                $ap_end = $end_date;
                foreach ($filtered as $value) {
                    if ($value->start <= $ap_start) {
                        if ($value->end > $ap_start) {
                            $ap_start = clone $value->end;
                            if ($value->end >= $ap_end) {
                                $ap_end = NULL;
                                break;
                            }
                        }
                    } elseif ($value->start < $ap_end) {
                        array_push($aps_date, [clone $ap_start, clone $value->start]);
                        if ($value->end < $ap_end) {
                            $ap_start = clone $value->end;
                        } else {
                            $ap_end = NULL;
                            break;
                        }
                    } else {
                        $ap_end = NULL;
                        break;
                    }
                }
                if ($ap_start !== NULL && $ap_end !== NULL) {
                    array_push($aps_date, [clone $ap_start, clone $ap_end]);
                }
            } else {
                array_push($aps_date, [clone $start_date, clone $end_date]);
            }
            
            foreach ($aps_date as $value) {
                if (date_diff($value[1], $value[0], true)->h > 0 || date_diff($value[1], $value[0], true)->h >= 15 ) {
                    $new_appointment = (object)[];
                    $new_appointment->start = $value[0];
                    $new_appointment->end = $value[1];
                    $new_appointment->status = true;
                    $new_appointment->id = $appointment['id'];
                    $new_appointment->title = $appointment['title'];
                    $new_appointment->textColor = $appointment['type'] == 10 ? $colors[2] : $colors[1];
                    $new_appointment->backgroundColor = '#fff';
                    $new_appointment->borderColor = '#fff';
                    $new_appointment->editable = false;
    //                $new_appointment->startEditable = false;
    //                $new_appointment->durationEditable = false;
    //                $new_appointment->resourceEditable = false;
                    $new_appointment->className = $appointment['type'] == 10 ? 'pointer available' : 'pointer unavailable';
                    $new_appointment->type = $appointment['type'];
                    array_push($result, $new_appointment);
                }
            }
        }

        return collect($result)->sortBy('start');
    }

    protected function arrangeTimeslot($date, $timeslots, $appointments) {
        $colors = config('constant.color_meeting');
        $result = [];
        $year = intval($date->format('Y'));
        $month = intval($date->format('n'));
        $day = intval($date->format('j'));
        foreach($timeslots as $timeslot) {
            $start_date = Carbon::createFromFormat('U', strtotime($timeslot['start_time']));
            $start_date->setDate($year, $month, $day);
            $end_date = Carbon::createFromFormat('U', strtotime($timeslot['end_time']));
            $end_date->setDate($year, $month, $day);

            $filtered = $appointments->filter(function($value, $key) use ($start_date, $end_date, $timeslot) {
                if ($timeslot['type'] == true) {
                    return ($value->start < $end_date && $value->end > $start_date);
                } else {
                    return ($value->start < $end_date && $value->end > $start_date);// && $value->type == 10);
                }
            })->all();

//             if (count($filtered) > 0) {
//                 continue;
//             } else {
//                 $appointment = (object)[];
//                 $appointment->start = $start_date;
//                 $appointment->end = $end_date;
//                 $appointment->status = true;
//                 $appointment->title = $timeslot['type'] == true ? 'Available Timeslot' : 'Unavailable Timeslot';
//                 $appointment->textColor = $timeslot['type'] == true ? $colors[2] : $colors[1];
//                 $appointment->backgroundColor = '#fff';
//                 $appointment->borderColor = '#fff';
//                 $appointment->editable = false;
// //                $appointment->startEditable = false;
// //                $appointment->durationEditable = false;
// //                $appointment->resourceEditable = false;
//                 $appointment->className = $timeslot['type'] == true ? 'pointer' : '';
//                 $appointment->type = $timeslot['type'] == true ? 12 : 13;
//                 array_push($result, $appointment);
//                 continue;
//             }

            $aps_date = [];

            if (count($filtered) > 0) {
                $ap_start = clone $start_date;
                $ap_end = clone $end_date;
                foreach ($filtered as $value) {
                    if ($value->start <= $ap_start) {
                        if ($value->end > $ap_start) {
                            $ap_start = clone $value->end;
                            if ($value->end >= $ap_end) {
                                $ap_end = NULL;
                                break;
                            }
                        }
                    } elseif ($value->start < $ap_end) {
                        array_push($aps_date, [clone $ap_start, clone $value->start]);
                        if ($value->end < $ap_end) {
                            $ap_start = clone $value->end;
                        } else {
                            $ap_end = NULL;
                            break;
                        }
                    } else {
                        $ap_end = NULL;
                        break;
                    }
                }
                if ($ap_start !== NULL && $ap_end !== NULL) {
                    array_push($aps_date, [clone $ap_start, clone $ap_end]);
                }
            } else {
                array_push($aps_date, [clone $start_date, clone $end_date]);
            }
            
            foreach ($aps_date as $value) {
                if (date_diff($value[1], $value[0], true)->h > 0 || date_diff($value[1], $value[0], true)->h >= 15 ) {
                    $appointment = (object)[];
                    $appointment->start = $value[0];
                    $appointment->end = $value[1];
                    $appointment->status = true;
                    $appointment->title = $timeslot['type'] == true ? 'Available for Appointment' : 'Unavailable for Appointment';
                    $appointment->textColor = $timeslot['type'] == true ? $colors[2] : $colors[1];
                    $appointment->backgroundColor = '#fff';
                    $appointment->borderColor = '#fff';
                    $appointment->editable = false;
    //                $appointment->startEditable = false;
    //                $appointment->durationEditable = false;
    //                $appointment->resourceEditable = false;
                    $appointment->className = $timeslot['type'] == true ? 'pointer available' : 'unavailable';
                    $appointment->type = $timeslot['type'] == true ? 12 : 13;
                    array_push($result, $appointment);
                }
            }
        }

        return collect($result);
    }

    public function createByClient(Request $request) {               
        $validator = Validator::make($request->all(), [
            'advisor_id' => 'required|integer|exists:users,id',
            'appointment_date' => 'required|date',
            'start_time' => 'required|string',
            'setby' => 'required|string|min:3|max:255',
            'name' => 'required|string|min:3|max:255',
            'address' => 'required|string|min:3|max:255',
            'city' => 'required|string|min:3|max:255',
            'state' => 'required|integer|max:255|exists:states,id',
            'zip_code' => 'required|string|min:3|max:16',
            'phone_number' => 'required|string|min:3|max:16',
            'email' => 'nullable|string|min:3|max:64',
            'length' => 'required|integer|min:0|max:4',
            'type' => 'required|integer|min:0|max:1',
            'description' => 'required|string',
            'agency' => 'required|string'
        ]);
        $validator->validate();
        

        $advisor = Auth::user()->advisors()->find($request->advisor_id);
        if ($advisor === NULL) {
            throw ValidationException::withMessages([
                'advisor_id' => [trans('error.invalid_advisor')],
            ]);
        }

        $appointment_date = Carbon::createFromFormat('m/d/Y', $request->appointment_date);
        $start_time = Carbon::createFromFormat('g:i A', $request->start_time);
        $start_date = clone $appointment_date;
        $start_date->setTime($start_time->format('G'), $start_time->format('i'));
        $end_date = clone $start_date;
        $end_date->add(\DateInterval::createfromdatestring(config('constant.length_meeting')[intval($request['length'])]));

        if ($this->checkAppointment(0, $advisor, $start_date, $end_date) == false) {
            throw ValidationException::withMessages([
                'start_time' => [trans('error.invalid_time')],
            ]);
        }

        $appointment = $advisor->appointments()->create([
//            'title' => config('constant.type_meeting')[intval($request['type'])],
            // 'title' => $advisor->pivot->agency->name . '-' . $request['city'],
            'title' => $request['agency'] . '-' . $request['city'],
            'start' => $start_date,
            'end' => $end_date,
            'setby' => $request['setby'],
            'name' => $request['name'],
            'address' => $request['address'],
            'city' => $request['city'],
            'state' => State::find($request['state'])->name,
            'zip_code' => $request['zip_code'],
            'phone_number' => $request['phone_number'],
            'email' => $request['email'],
            'length' => $request['length'],
            'type' => $request['type'],
            'description' => $request['description'],
            'client_id' => Auth::user()->id,
            'status' => true,
            'agency' => $request['agency'],
        ]);
        
        

        $receivers = [$appointment->advisor];
        foreach($appointment->advisor->clients as $client) {
            array_push($receivers, $client);
        }
        
        Notification::send($receivers, new AppointmentEmail($appointment));

        return back()->with('success', 'Appointment successfully Created');
    }

    public function createByAdvisor(Request $request) {               
        $validator = Validator::make($request->all(), [
            'appointment_date' => 'required|date',
            'start_time' => 'required|string',
            'setby' => 'required|string|min:3|max:255',
            'client_id' => 'required|integer|exists:users,id',
            'address' => 'required|string|min:3|max:255',
            'city' => 'required|string|min:3|max:255',
            'state' => 'required|integer|max:255|exists:states,id',
            'zip_code' => 'required|string|min:3|max:16',
            'name' => 'required|string|min:3|max:255',
            'phone_number' => 'required|string|min:3|max:16',
            'email' => 'nullable|string|min:3|max:64',
            'length' => 'required|integer|min:0|max:4',
            'type' => 'required|integer|min:0|max:1',
            'description' => 'required|string',
            'agency' => 'required|string'

        ]);
        if (!$validator->passes())
        {
            return response()->json(['errors' => $validator->errors()]);
        }

        // $client = Auth::user()->clients()->find($request->client_id);
        // if ($client === NULL) {
        //     return response()->json(['client_id' => 'Invalid User']);
        // }        
        

        $appointment_date = Carbon::createFromFormat('m/d/Y', $request->appointment_date);
        $start_time = Carbon::createFromFormat('g:i A', $request->start_time);
        $start_date = clone $appointment_date;
        $start_date->setTime($start_time->format('G'), $start_time->format('i'));
        $end_date = clone $start_date;
        $end_date->add(\DateInterval::createfromdatestring(config('constant.length_meeting')[intval($request['length'])]));

        if ($this->checkAppointment(0, Auth::user(), $start_date, $end_date) == false) {
            return response()->json(['start_time' => trans('error.invalid_time')]);
        }

        $appointment = Auth::user()->appointments()->create([
//            'title' => config('constant.type_meeting')[intval($request['type'])],
            // 'title' => $client->pivot->agency->name . '-' . $request['city'],
            'title' => $request['agency'] . '-' . $request['city'],
            'start' => $start_date,
            'end' => $end_date,
            'setby' => $request['setby'],
            'name' => $request['name'],
            'address' => $request['address'],
            'city' => $request['city'],
            'state' => State::find($request['state'])->name,
            'zip_code' => $request['zip_code'],
            'phone_number' => $request['phone_number'],
            'email' => $request['email'],
            'length' => $request['length'],
            'type' => $request['type'],
            'description' => $request['description'],
            // 'client_id' => $client->id,
            'client_id' => $request['client_id'],
            'status' => true,
            'agency' => $request['agency'],
        ]);

        $receivers = [$appointment->advisor];        
        foreach($appointment->advisor->clients as $client) {
            array_push($receivers, $client);
        }
        
        Notification::send($receivers, new AppointmentEmail($appointment));

        

        return response()->json(['success' => 'Appointment successfully Created']);
    }

    protected function checkAppointment($type, $advisor, $start_date, $end_date, $appointment_id = NULL) {
        // unavailable check
        // check other appointment
        if ($appointment_id == NULL) {
            if ($advisor->appointments()->whereNotNull('client_id')->where('end', '>', $start_date)->where('start', '<', $end_date)->first() != NULL) {
                return false;
            }
        } else {
            if ($advisor->appointments()->whereNotNull('client_id')->where('id', '<>', $appointment_id)->where('end', '>', $start_date)->where('start', '<', $end_date)->first() != NULL) {
                return false;
            }
        }
           
        // check unavailable time appointment
        if ($appointment_id == NULL && $advisor->appointments()->whereNull('client_id')->where('type', 11)->where('end', '>', $start_date)->where('start', '<', $end_date)->first() != NULL) {
            return false;
        }

        $weekday = $start_date->format('w');

        // check unavailable timeslot
        // if ( $advisor->timeslots()->where('type', false)->where('weekday', $weekday)->where('end_time', '>', $start_date)->where('start_time', '<', $end_date)->first() != NULL) {
        //     return false;
        // }

        // available check           
        // check available time appointment
        if ($advisor->appointments()->whereNull('client_id')->where('type', 10)->where('end', '>=', $end_date)->where('start', '<=', $start_date)->first() == NULL) {
            // check available timeslot
            if ($type == 0) {
                if ($advisor->timeslots()->where('type', true)->where('weekday', $weekday)->where('end_time', '>=', $end_date)->where('start_time', '<=', $start_date)->first() == NULL) {
                    return false;
                }
            }
        }

        return true;
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:appointments',
            'start' => 'required_with:end|integer',
            'end' => 'required_with:start|integer',
            'start_time' => 'required_without:start,end|string',
            'setby' => 'required|string|min:3|max:255',
            'name' => 'required|string|min:3|max:255',
            'address' => 'required|string|min:3|max:255',
            'city' => 'required|string|min:3|max:255',
            'state' => 'required|integer|exists:states,id',
            'zip_code' => 'required|string|min:3|max:16',
            'phone_number' => 'required|string|min:3|max:16',
            'email' => 'nullable|string|min:3|max:64',
            'length' => 'nullable|integer|min:0|max:4',
            'type' => 'required|integer|min:0|max:1',
            'status' => 'nullable|boolean',
            'description' => 'required_without:start_time|string',
        ]);
        if (!$validator->passes())
        {
            return response()->json(['errors' => $validator->errors()]);
        }
        $appointment = Appointment::find($request->id);
        $user = Auth::user();
        if (($user->isClient() && $appointment->client_id != $user->id) || ($user->isAdvisor() && $appointment->advisor_id != $user->id)) {
            return response()->json(['client_id' => 'Invalid User']);
        }

        if ($request->start != NULL && $request->end != NULL) {
            $start_date = Carbon::createFromFormat('U', $request->start);
            $end_date = Carbon::createFromFormat('U', $request->end);
        
            if ($this->checkAppointment(1, ($user->isClient() ? $appointment->advisor : $user), $start_date, $end_date, $request->id) == false) {
                return response()->json(['start_time' => trans('error.invalid_time')]);
            }

            $appointment->start = $start_date;
            $appointment->end = $end_date;
        } elseif ($request->start_time != NULL) {
            $temp = explode("/", $request->select_date);
            $date = '';
            $date .= $temp[2] . '-' . $temp[0] . '-' . $temp[1] . ' ' . '00:00:00';
            $start_date = Carbon::createFromFormat('Y-m-d h:i:s', $date);
            $start_time = Carbon::createFromFormat('g:i A', $request->start_time);
            $start_date->setTime($start_time->format('G'), $start_time->format('i'));
            $end_date = clone $start_date;
            $end_date->add(\DateInterval::createfromdatestring(config('constant.length_meeting')[$appointment->length]));
        
            if ($this->checkAppointment(1, ($user->isClient() ? $appointment->advisor : $user), $start_date, $end_date, $request->id) == false) {
                return response()->json(['start_time' => trans('error.invalid_time')]);
            }

            $appointment->start = $start_date;
            $appointment->end = $end_date;
        } else {
            return response()->json(['start_time' => trans('error.invalid_time')]);
        }

        $appointment->setby = $request['setby'];
        $appointment->name = $request['name'];
        $appointment->address = $request['address'];
        $appointment->city = $request['city'];
        $appointment->state = State::find($request['state'])->name;
        $appointment->zip_code = $request['zip_code'];
        $appointment->phone_number = $request['phone_number'];
        $appointment->email = $request['email'];
        $appointment->length = $request->length ? $request['length'] : $appointment->length;
        $appointment->type = $request['type'];
        $appointment->status = $request->status ?: $appointment->status;
        $appointment->description = $request->description ?: $appointment->description;
        $appointment->save();

        $timestamp = $appointment->start->getTimestamp();
        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true, 'timestamp'=>$timestamp]);
        }
       
        return response()->json(['success' => 'Appointment successfully Created', 'timestamp'=>$timestamp]);
    }
    
    public function upgrade(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:appointments',
            'start_time' => 'required|string',
//            'status' => 'required|integer',
        ]);
        $validator->validate();
        
        $appointment = Appointment::find($request->id);
        $user = Auth::user();
        if (($user->isClient() && $appointment->client_id != $user->id) || ($user->isAdvisor() && $appointment->advisor_id != $user->id)) {
            throw ValidationException::withMessages([
                'id' => [trans('error.invalid_appointment')],
            ]);
        }
        
        $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $appointment->start);
        $start_time = Carbon::createFromFormat('g:i A', $request->start_time);
        $start_date->setTime($start_time->format('G'), $start_time->format('i'));
        $end_date = clone $start_date;
        $end_date->add(\DateInterval::createfromdatestring(config('constant.length_meeting')[$appointment->length]));
    
        if ($this->checkAppointment(0, ($user->isClient() ? $appointment->advisor : $user), $start_date, $end_date, $request->id) == false) {
            throw ValidationException::withMessages([
                'start_time' => [trans('error.invalid_time')],
            ]);
        }

        $appointment->start = $start_date;
        $appointment->end = $end_date;
        $appointment->status = true;
        $appointment->save();

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }

        return back();
    }
    
    public function change(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:appointments',
        ]);
        $validator->validate();
        
        $appointment = Appointment::find($request->id);
        $user = Auth::user();
        if (($user->isClient() && $appointment->client_id != $user->id) || ($user->isAdvisor() && $appointment->advisor_id != $user->id)) {
            throw ValidationException::withMessages([
                'id' => [trans('error.invalid_appointment')],
            ]);
        }
        if ($appointment->type == 10) {
            $appointment->title = 'Unavailable for Appointment';
            $appointment->type = 11;
        } elseif ($appointment->type == 11) {
            $appointment->title = 'Available for Appointment';
            $appointment->type = 10;
        }
        $appointment->save();
        
        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }

        return back();
    }
    
    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:appointments',
        ]);
        $validator->validate();
        
        $appointment = Appointment::find($request->id);
        $user = Auth::user();
        if (($user->isClient() && $appointment->client_id != $user->id) || ($user->isAdvisor() && $appointment->advisor_id != $user->id)) {
            throw ValidationException::withMessages([
                'id' => [trans('error.invalid_appointment')],
            ]);
        }
        $appointment->delete();
        
        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }

        return back();
    }
    
    public function forceDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:appointments,id',
        ]);
        $validator->validate();
        
        $appointment = Appointment::find($request->id);
        $user = Auth::user();
        if (($user->isClient() && $appointment->client_id != $user->id) || ($user->isAdvisor() && $appointment->advisor_id != $user->id)) {
            throw ValidationException::withMessages([
                'id' => [trans('error.invalid_appointment')],
            ]);
        }
        $appointment->forceDelete();
        
        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }

        return back();
    }
    
    public function schedule($timeStamp = NULL)
    {
        $lengths = config('constant.length_meeting');
        $types = config('constant.type_meeting'); 
        $states = State::all();

        if ($timeStamp === NULL) {
            $date = new Carbon();
            $timeStamp = $date->setTime(0,0,0)->getTimeStamp();
            $timezone = Auth::user()->emailSetting->timezone;
           // $timeStamp = $timeStamp - $timezone * 60;
        }
        $start_day = Carbon::createFromFormat('U', $timeStamp)->setTime(0, 0, 0);
        $end_day = Carbon::createFromFormat('U', $timeStamp)->setTime(23, 59, 59);
//        $title = $start_day->format('l F j, Y');
        
        $appointments = Auth::user()->appointments()->where('start', '<', $end_day)->where('end', '>', $start_day)->orderBy('start')->orderBy('end')->get();
        
        foreach ($appointments as $value) {
            $value->start = Carbon::createFromFormat('Y-m-d H:i:s', $value->start);
            $value->end = Carbon::createFromFormat('Y-m-d  H:i:s', $value->end);
        }
        
        $appointments = $this->arrangeAppointments($start_day, $appointments);

//        $weekday = Carbon::createFromFormat('U', $timeStamp)->format('w');
        $weekday = date('w', $timeStamp);
        $timeslots = Auth::user()->timeslots()->where('weekday', $weekday)->orderBy('start_time', 'asc')->get();
        $result = $this->arrangeTimeslot($start_day, $timeslots, $appointments);
        $clients = Auth::user()->clients()->get();
        
        $user = Auth::user();        
        foreach ($clients as $client) {
            $client->pivot->agency;    
            
        }        
                                
        $calendarSetting = Auth::user()->calendarSetting;
        $calendar_setting = ($calendarSetting && $calendarSetting->calendar_setting) ? $calendarSetting->calendar_setting : 4;
        return view('advisor.schedule', [
            'title' => $start_day,
            'appointments' => $appointments->merge($result)->sortBy('start'),
            'timeStamp' => $timeStamp, 
            'lengths' => $lengths, 
            'types' => $types,
            'states' => $states,
            'clients' => $clients,
            'calendar_setting' => $calendar_setting,
        ]);
    }
    
    public function createFake(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'timestamp' => 'required|integer',
            'start_time' => 'required|string',
            'end_time' => 'required|string',
            'type' => 'required|boolean', // true: available, false: unavailable
            // 'timezone' => 'required|integer',
        ]);
        $validator->validate();
        
        $time = Carbon::createFromFormat('U', $request->timestamp);
        $start_time = Carbon::createFromFormat('g:i A', $request->start_time);
        $end_time = Carbon::createFromFormat('g:i A', $request->end_time);
        $start = clone $time->setTime($start_time->format('G'), $start_time->format('i'));
        $end = clone $time->setTime($end_time->format('G'), $end_time->format('i'));
        
        $user = Auth::user();
        if ($start_time >= $end_time) {
            throw ValidationException::withMessages([
                'time' => [trans('error.invalid_time')],
            ]);
        }
        $appointment = $user->appointments()->create([
            'title' => $request->type == true ? 'Available for Appointment' : 'Unavailable for Appointment',
            'start' => $start,
            'end' => $end,
            'type' => $request->type == true ? 10 : 11,
            'status' => true,
        ]);
        
        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }

        return back();
    }
    
    public function timeslot()
    {
        $user = Auth::user();
        $timeslots = $user->timeslots()->orderBy('start_time', 'asc')->get();
        foreach ($timeslots as $timeslot) {
            $timeslot->start_time = date('h:i A', strtotime($timeslot->start_time));
            $timeslot->end_time = date('h:i A', strtotime($timeslot->end_time));
        }
        $timeslots = $timeslots->groupBy('weekday')->toArray();
        $weekday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        
        return view('advisor.timeslot', ['timeslots' => $timeslots, 'weekday' => $weekday]);
    }
    
    public function createTimeslot(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'weekday' => 'required|integer|min:0|max:6',
            'start_time' => 'required|string',
            'end_time' => 'required|string',
            'type' => 'required|boolean',
            // 'timezone' => 'required|integer',
        ]);
        $validator->validate();
        
        $start_time = Carbon::createFromFormat('h:i A', $request->start_time);
        $end_time = Carbon::createFromFormat('h:i A', $request->end_time);
        
        $user = Auth::user();
        if ($start_time >= $end_time || $user->timeslots()->where('weekday', $request->weekday)->where('end_time', '>', $start_time)->where('start_time', '<', $end_time)->first() != NULL) {
            throw ValidationException::withMessages([
                'time' => [trans('error.invalid_time')],
            ]);
        }
        $timeslot = $user->timeslots()->create(
                [
                    'weekday' => $request->weekday,
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'type' => $request->type,
                    // 'timezone' => $request->timezone,
                ]);
        
        return response()->json(['success' => true, 'timeslot' => $timeslot]);
    }
    
    public function deleteTimeslot(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:timeslots',
        ]);
        $validator->validate();
                
        $user = Auth::user();
        $timeslot = Timeslot::find($request->id);        
        if ($user->id != $timeslot->user->id) {
            throw ValidationException::withMessages([
                'id' => [trans('error.invalid_timeslot')],
            ]);
        }
        $timeslot->delete();
        
        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }

        return back();
    }
}
