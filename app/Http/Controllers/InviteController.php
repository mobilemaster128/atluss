<?php

namespace App\Http\Controllers;

use App\User;
use App\Invite;
use Carbon\Carbon;
use App\Notifications\InviteEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class InviteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('showAcceptForm');
    }

    public function index()
    {
        $agencies = Auth::user()->agencies;

        return view('invite.create', ['agencies' => $agencies]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'agency' => 'required|string|min:2|max:255',
            // 'g-recaptcha-response' => 'required|recaptcha',
        ]);
        $validator->validate();

        $user = User::where('email', $request->email)->first();
        if ($user !== NULL && !$user->isClient()) {
            throw ValidationException::withMessages([
                'email' => [trans('invite.invalid_invite')],
            ]);
        }

//        $invite = Auth::user()->invites()->where('email', $request->email)->first();
//
//        if ($invite !== NULL) {
//            throw ValidationException::withMessages([
//                'email' => [trans('invite.invalid_invite')],
//            ]);
//        }

        $agency = Auth::user()->agencies()->updateOrCreate(['name' => $request->agency], []);

        $invite = Auth::user()->invites()->updateOrCreate(
            [
                'email' => $request['email'],
            ],
            [
                'token' => $this->generateToken(),
                'agency_id' => $agency->id,
                'expires_at' => now(config('app.timezone'))->addDays(30),
            ]
        );

        Notification::route('mail', $invite->email)
                    ->notify(new InviteEmail($invite));

        return back()->with('success', trans('invite.success'));
    }

    public function showAcceptForm($id, $token)
    {
        $invite = Invite::find($id);
        if ($invite === NULL || $invite->token != $token || $invite->expires_at < Carbon::now()) {
            return abort('400', 'Invalid invitation');
        }

//        $user = Auth::user('web');
        $user = Auth::user();
        if ($user !== NULL) {
            if (!$user->isClient() || $user->email != $invite->email) {
                return abort('400', 'Invalid invitation');
            }
            if ($user->confirmed == FALSE) {
                $user->confirmed = true;
                $user->save();
            }

            $invite = Invite::find($id);
            $invite->delete();

            $user->advisors()->attach($invite->user, ['agency_id' => $invite->agency_id,]);

            return redirect("client/calendar/$invite->user_id"); 
        }       
        
        $user = User::where('email', $invite->email)->first();
        if ($user !== NULL) {
            if ($user->isClient()) {
                return view('auth.login', ['invite' => $invite])->with('success', trans('invite.login_accept'));
            } else {
                return view('auth.login')->with('danger', trans('invite.invalid_invite'));
            }
        }

        return view('auth.register', ['invite' => $invite])->with('success', trans('invite.register_accept'));
    }
    
    public function resendInvite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:invites',
        ]);
        $validator->validate();

        $user = Auth::user();
        $invite = $user->invites()->find($request->id);

        if ($invite !== NULL) {
            $invite->token = $this->generateToken();
            $invite->expires_at = now(config('app.timezone'))->addDays(30);
            $invite->save();

            Notification::route('mail', $invite->email)
                        ->notify(new InviteEmail($invite));
        }

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }

        return back()->with('success', trans('invite.success'));
    }
    
    public function updateInvite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:invites',
            'agency' => 'required|string|exists:agencies,id',
        ]);
        $validator->validate();

        $user = Auth::user();
        $invite = $user->invites()->find($request->id);

        $agency = $user->agencies()->find($request->agency);
        if ($agency === NULL) {
            throw ValidationException::withMessages([
                'agency' => ['Invalid Agency'],
            ]);
        }

        if ($invite !== NULL) {
            $invite->agency_id = $agency->id;
            $invite->save();
        }

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }
    
        return back()->with('success', trans('invite.success'));
    }
    
    public function removeInvite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:invites',
        ]);
        $validator->validate();

        $user = Auth::user();
        $invite = $user->invites()->find($request->id);

        if ($invite !== NULL) {
            $invite->delete();
        }

        if ($request->wantsJson()) {  // Accept=>application/json in Header
        // if ($request->isJson()) {  // CONTENT_TYPE=>application/json in Header
            return response()->json(['success' => true]);
        }
    
        return back()->with('success', trans('invite.success'));
    }
    
    protected function generateToken() {
        $token = md5(uniqid());
        if (Invite::where('token', $token)->first()) {
            return $this->generateToken();
        }
        return $token;
    }
}
