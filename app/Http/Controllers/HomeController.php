<?php

namespace App\Http\Controllers;

use App\Faq;
use App\ContactUs;
use App\Mail\ContactUSMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use Carbon\Carbon;
use App\User;
use App\Appointment;
use App\Notifications\DailyEmail;
use App\Notifications\ReminderEmail;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('guest.home', ['home' => true]);
    }

    public function about()
    {
        return view('guest.about', ['about' => true]);
    }

    public function showContactusForm()
    {
        return view('guest.contactus', ['contactus' => true]);
    }

    public function contactus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|min:3|max:64',
            'email' => 'required|string|email|max:64',
            'phone_number' => 'required|string|min:9|max:16',
            'message' => 'required|string|min:6',
            'g-recaptcha-response' => 'required',
        ]);
        $validator->validate();

        $contactus = ContactUs::create($request->all());

        Mail::to('mailto@atluss.com')
            ->send(new ContactUSMail($contactus));

        return back()->with('status', 'Thanks for contacting us!');
    }

    public function faq()
    {
        $faqs = Faq::all();
        return view('guest.faq', ['faqs' => $faqs, 'faq' => true]);
    }
    
    public function pricing()
    {
        return view('guest.pricing', ['pricing' => true]);
    }

    public function daily() {
        // send now time
        $time = now();
        $min = intval($time->format('i'));// >= 30 ? 30 : 0;
        $hour = intval($time->format('G'));
        $time->setTime($hour, $min, 0);
        $advisors = User::where('type', 2)->whereHas('emailSetting', function($query) use ($time){
            $query->where('daily_time', $time);
        })->get();
        foreach($advisors as $advisor) {
            $receivers = [];
            foreach($advisor->clients as $client) {
                array_push($receivers, $client);
            }
            if (count($receivers) > 0) {
                Notification::send($receivers, new DailyEmail($advisor));
            }
        }

        return response()->json(['success' => $time]);
    }

    public function reminder() {
        $time = now();
        $min = intval($time->format('i'));
        $min = $min >= 45 ? 45 : ($min >= 30 ? 30 : ($min >= 15 ? 15 : 0));
        $hour = intval($time->format('G'));
        $time->setTime($hour, $min, 0);
        $appointments = Appointment::whereNotNull('client_id')->where('start', '>', $time)->get();
        foreach($appointments as $appointment) {
            $start = Carbon::createFromFormat('Y-m-d H:i:s', $appointment->start);
            $advisor = $appointment->advisor;
            $prior_time = $advisor->emailSetting == NULL ? 24 : ($advisor->emailSetting->prior_time ?: 24);
            $timezone = $advisor->emailSetting == NULL ? 0 : ($advisor->emailSetting->timezone ?: 0);
            $start->add(\DateInterval::createfromdatestring("$timezone minutes"));
            $start->sub(\DateInterval::createfromdatestring("$prior_time hours"));
            if ($start == $time) {
                $receivers = [$appointment->advisor, $appointment->client];
                Notification::send($receivers, new ReminderEmail($appointment));
            }
        }

        return response()->json(['success' => $time]);
    }
}