<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index()
    {
        return view('subscription');
    }

    public function store(Request $request)
    {
        $controller = new UserController();
        return $controller->profile($request->input('selector'));
    }
}
