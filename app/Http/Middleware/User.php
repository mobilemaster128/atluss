<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->isClient()) {
                return $next($request);
            } else if (Auth::user()->isAdvisor()) {
                if ($request->user() && !$request->user()->subscribed('main')) {
                    return redirect('subscribe');
                } else {
                    return $next($request);
                }
            }
        }
        
        return redirect('home');
    }
}
