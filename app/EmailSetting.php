<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailSetting extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'user_id', 'daily_content', 'daily_time', 'appointment_content', 'prior_time', 'reminder_content', 'timezone',
    ];
    
    protected $hidden = [
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
