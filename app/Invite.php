<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invite extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'email', 'token', 'user_id', 'expires_at', 'agency_id',
    ];

    protected $hidden = [
        'token', 'deleted_at',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function agency()
    {
        return $this->belongsTo('App\Agency');
    }
}
