<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class AdvisorClient extends Pivot
{
    protected $table = 'advisor_client';

    public $timestamps = false;
    
    protected $fillable = [
        'advisor_id', 'client_id', 'agency_id',
    ];

    protected $hidden = [
    ];

    public function advisor()
    {
        return $this->belongsTo('App\User', 'advisor_id');
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function agency()
    {
        return $this->belongsTo('App\Agency');
    }
}
