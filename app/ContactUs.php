<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = "contact_us";
    
    protected $fillable = [
        'full_name', 'email', 'phone_number', 'message',
    ];
    
    protected $hidden = [
    ];
}
