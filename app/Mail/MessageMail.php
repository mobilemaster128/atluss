<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $message;
    public $link;
    public $user_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message, $subject, $link, $user_name)
    {
        $this->message = $message;        
        $this->subject = $subject;
        $this->link = $link;
        $this->user_name = $user_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        return $this->view('email.message')
                ->with(['data' => $this->message])
                ->with(['link' => $this->link])
                ->with(['name' => $this->user_name]);
    }
}
