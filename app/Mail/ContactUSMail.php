<?php

namespace App\Mail;

use App\ContactUs;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUSMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $contactus;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactUs $contactus)
    {
        $this->contactus = $contactus;
        $this->subject = "Contact US";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.contactus')
                ->with(['data' => $this->contactus]);
    }
}
