<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $name;
    public $email;
    public $created_at;    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $created_at)
    {
        $this->name = $name;        
        $this->email = $email;
        $this->created_at = $created_at;
        $this->subject = "New User Registration";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.registration')
                ->with(['name' => $this->name])
                ->with(['email' => $this->email])
                ->with(['created_at' => $this->created_at]);
    }
}
