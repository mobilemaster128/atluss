<?php

namespace App\Console;

use Carbon\Carbon;
use App\User;
use App\Appointment;
use App\Notifications\DailyEmail;
use App\Notifications\ReminderEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {        
        $schedule->call(function () {
            // send now time
            $time = now();
            $min = intval($time->format('i'));// >= 30 ? 30 : 0;
            $hour = intval($time->format('G'));
            $time->setTime($hour, $min, 0);
            $advisors = User::where('type', 2)->whereHas('emailSetting', function($query) use ($time){
                $query->where('daily_time', $time);
            })->get();
            foreach($advisors as $advisor) {
                $receivers = [];
                foreach($advisor->clients as $client) {
                    array_push($receivers, $client);
                }
                array_push($receivers, $advisor);
                
                if (count($receivers) > 0) {
                    Notification::send($receivers, new DailyEmail($advisor));
                }
            }
        })->everyMinute();

        $schedule->call(function () {
            // send 7:30 AM
            $time = now();
            $min = intval($time->format('i')) >= 30 ? 30 : 0;
            $hour = intval($time->format('G'));
            $time->setTime($hour, $min, 0);
            $advisors = User::where('type', 2)->whereHas('emailSetting', function($query) {
                $query->whereNull('daily_time');
            })->get();
            foreach($advisors as $advisor) {
                $timezone = $advisor->emailSetting->timezone;
                $start_time = clone $time;
                $start_time->setTime(7, 30, 0);
                $start_time->add(\DateInterval::createfromdatestring("$timezone minutes"));
                if ($time == $start_time) {
                    $receivers = [];
                    foreach($advisor->clients as $client) {
                        array_push($receivers, $client);
                    }
                    array_push($receivers, $advisor);

                    if (count($receivers) > 0) {
                        Notification::send($receivers, new DailyEmail($advisor));
                    } 
                }
            }
        })->everyThirtyMinutes();

        // reminder email
        $schedule->call(function () {
            $time = now();
            $min = intval($time->format('i'));
            $min = $min >= 45 ? 45 : ($min >= 30 ? 30 : ($min >= 15 ? 15 : 0));
            $hour = intval($time->format('G'));
            $time->setTime($hour, $min, 0);
            $appointments = Appointment::whereNotNull('client_id')->where('start', '>', $time)->get();
            foreach($appointments as $appointment) {
                $start = Carbon::createFromFormat('Y-m-d H:i:s', $appointment->start);
                $advisor = $appointment->advisor;
                if ($advisor) {
                    $prior_time = $advisor->emailSetting == NULL ? 24 : ($advisor->emailSetting->prior_time ?: 24);
                    $timezone = $advisor->emailSetting == NULL ? 0 : ($advisor->emailSetting->timezone ?: 0);
                    $start_time = clone $time;
                    $start_time->sub(\DateInterval::createfromdatestring("$timezone minutes"));
                    $start_time->add(\DateInterval::createfromdatestring("$prior_time hours"));
                    if ($start == $start_time) {
                        $receivers = [$appointment->advisor, $appointment->client];
                        Notification::send($receivers, new ReminderEmail($appointment));
                    }
                }
            }
        })->everyFifteenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
