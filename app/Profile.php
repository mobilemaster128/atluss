<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'card_name', 'card_number', 'expires_at', 'cvc', 'user_name', 'address', 'city', 'state', 'zip_code', 'user_id',
    ];
    
    protected $hidden = [
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
