<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'company', 'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin() {
        if ($this->type == 1) {
            return true;
        }
        return false;
    }

    public function isAdvisor() {
        if ($this->type == 2) {
            return true;
        }
        return false;
    }

    public function isClient() {
        if ($this->type == 3) {
            return true;
        }
        return false;
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function emailSetting()
    {
        return $this->hasOne('App\EmailSetting');
    }

    public function calendarSetting()
    {
        return $this->hasOne('App\CalendarSetting');
    }

    public function appointments()
    {
        if ($this->isAdvisor()) {
            return $this->hasMany('App\Appointment', 'advisor_id');
        } else {
            return $this->hasMany('App\Appointment', 'client_id');
        }
    }

    public function agencies()
    {
        return $this->hasMany('App\Agency', 'advisor_id');
    }

    public function invites()
    {
        return $this->hasMany('App\Invite');
    }

    public function timeslots()
    {
        return $this->hasMany('App\Timeslot');
    }

    public function clients()
    {
        return $this->belongsToMany('App\User', 'advisor_client', 'advisor_id', 'client_id')
                    ->withPivot('agency_id')
                    ->using('App\AdvisorClient');
    }

    public function advisors()
    {
        return $this->belongsToMany('App\User', 'advisor_client', 'client_id', 'advisor_id')
                    ->withPivot('agency_id')
                    ->using('App\AdvisorClient');
    }
}
