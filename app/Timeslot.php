<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timeslot extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'weekday', 'start_time', 'end_time', 'user_id', 'type',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
