<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        Paginator::useBootstrapThree();

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add(trans('menu.account_settings'));
            $event->menu->add([
                'text' => trans('menu.change_password'),
                'url' => 'password/change',
                'icon' => 'lock',
            ]);
            if (Auth::user()->isAdmin()) {
                $event->menu->add(trans('menu.dashboard'));
                $event->menu->add([
                    'text' => trans('menu.dashboard'),
                    'url' => 'admin/dashboard',
                    'icon' => 'list',
                ]);
                $event->menu->add([
                    'text' => trans('menu.user_manage'),
                    'url' => 'admin/users',
                    'icon' => 'list',
                ]);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
