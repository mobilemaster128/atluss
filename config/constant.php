<?php

return [

    'length_meeting' => [
        '15 minutes',
        '30 minutes',
        '45 minutes',
        '1 hour',
        '2 hours',
    ],
    
    'type_meeting' => [
        'Office Meeting',
        'Phone Meeting',
    ],

    'color_meeting' => [
        '#000000', // Office Meeting
        '#be0000', // Unavailable
        '#762696', // Available
        '#00cced', // Phone Meeting
        '#c0392b',
        '#8e44ad',
        '#2ecc71',
        '#0097e6',
        '#4cd137',
    ],
];
