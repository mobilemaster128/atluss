<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
        'card_name' => $faker->creditCardType,
        'card_number' => $faker->creditCardNumber,
        'expires_at' => Carbon::now()->addDays(rand(10, 30)),
        'cvc' => rand(100, 999),
        'user_name' => $faker->name,
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => App\State::inRandomOrder()->first()->name,
        'zip_code' => $faker->postcode,
    ];
});
