<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 64);
            $table->string('setby', 64)->nullable();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('city', 32)->nullable();
            $table->string('state', 16)->nullable();
            $table->string('zip_code', 16)->nullable();
            $table->string('phone_number', 16)->nullable();
            $table->string('email', 64)->nullable();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->integer('length')->nullable();
            $table->integer('type');  // 0: Office Meeting, 1: Phone Meeting, 10: Available by Advisor, 11: Unavailable by Advisor, 12: Available by Timeslot, 13: Unavailable by Timeslot 
            $table->text('description')->nullable();
            $table->integer('advisor_id');
            $table->integer('client_id')->nullable();
            $table->boolean('status')->default(false); // true: approved, false: pending
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
