<?php

use App\State;
use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info("Creating State table.");

        $json = File::get('database/data/state.json');
        $states = json_decode($json);
        foreach ($states as $state) {
            State::create([
                'name' => $state->name,
                'code' => $state->code,
            ]);
        }

        $this->command->info("City State created.");
    }
}
