<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StateTableSeeder::class);

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'company' => 'Atluss',
            'type' => 1,
            'confirmed' => true,
        ]);

        DB::table('users')->insert([
            'name' => 'Advisor',
            'email' => 'advisor@gmail.com',
            'password' => bcrypt('password'),
            'company' => 'Atluss',
            'type' => 2,
            'confirmed' => true,
        ]);

        DB::table('users')->insert([
            'name' => 'Client',
            'email' => 'client@gmail.com',
            'password' => bcrypt('password'),
            'company' => 'Atluss',
            'type' => 3,
            'confirmed' => true,
        ]);

        App\User::where('type', 2)->first()->profile()->save(factory(App\Profile::class)->make());
        App\User::where('type', 2)->first()->agencies()->create(['name' => 'Agency']);
        App\User::where('type', 2)->first()->clients()->attach(App\User::where('type', 3)->first(), 
            ['agency_id' => 1]);


        // create Faqs
        DB::table('faqs')->insert([
            'question' => 'Why the name Atluss?',
            'answer' => 'The word atlas has two meanings. It is a collection of maps that show a complete picture of a subject. It also is the most superior vertebra of the spine, named for Atlas of Greek mythology it supports the globe of the head, allowing it to turn side-to-side. For you, Atluss is an automated scheduling system designed to give you a complete picture of your calendar, ensuring that customers don’t get lost along the way, supporting you as the head of your business and helping you steer your ship in the right direction.',
        ]);
        DB::table('faqs')->insert([
            'question' => 'Is the data I provide to Atluss distributed to a third party?',
            'answer' => 'Unlike other connectivity platforms, we will not sell or distribute your data in order to influence or alter the outcomes of any elections! (JK) No, the data is not distributed.',
        ]);
        DB::table('faqs')->insert([
            'question' => 'Can all users see referral names/contact info?',
            'answer' => 'No, only the office/agency that set the appointment is privy to the referral private/contact information.',
        ]);
        DB::table('faqs')->insert([
            'question' => 'Is there a cost to my users?',
            'answer' => 'No, with your subscription, you may have up to XX users at no additional cost.',
        ]);

        // create fake users and profile
        // factory(App\User::class, 3)->create()->each(function ($user) {
        //     $user->profile()->save(factory(App\Profile::class)->make());
        // });
    }
}
