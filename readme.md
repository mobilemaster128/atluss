## About PaiServer

# git url
https://mobilemaster128@bitbucket.org/mobilemaster128/atluss.git

## Installation
git clone https://mobilemaster128@bitbucket.org/mobilemaster128/atluss.git
php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=assets

## bower install
sudo npm i -g bower
sudo npm i
sudo bower i --allow-root

## Deployment
php artisan passport:keys
composer install --optimize-autoloader
php artisan config:cache
php artisan route:cache