<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/about', 'HomeController@about');

Route::get('/contactus', 'HomeController@showContactusForm');
Route::post('/contactus', 'HomeController@contactus');

Route::get('/faq', 'HomeController@faq');

Route::get('/pricing', 'HomeController@pricing');

Route::get('/invite/accept/{id}/{token}', 'InviteController@showAcceptForm');

Route::middleware(['guest'])->group(function () {
    Route::post('/register/accept', 'Auth\RegisterController@accept');
    Route::post('/login/accept', 'Auth\LoginController@accept');
});

Route::middleware(['auth', 'advisor'])->group(function () {
    Route::get('/user/profile', 'UserController@profile')->name('profile');
    Route::post('/user/profile/set', 'UserController@setProfile');
    Route::post('/user/profile/update', 'UserController@updateProfile');
    Route::post('/user/profile/cancel', 'UserController@cancelProfile');
    
    Route::post('/user/email/change', 'Auth\RegisterController@changeEmail');
    
    Route::get('/user/invite', 'InviteController@index');
    Route::post('/user/invite', 'InviteController@create');
    Route::post('/invite/resend', 'InviteController@resendInvite');

    Route::get('/calendar', 'AppointmentController@index');
    Route::post('/appointment/search', 'AppointmentController@search');
    Route::post('/appointment/create', 'AppointmentController@create');
    Route::post('/appointment/update', 'AppointmentController@update');
    Route::post('/appointment/change', 'AppointmentController@change');
    Route::post('/appointment/upgrade', 'AppointmentController@upgrade');
    Route::post('/appointment/create', 'AppointmentController@createByAdvisor');
    Route::post('/appointment/create/fake', 'AppointmentController@createFake');
    Route::post('/appointment/delete', 'AppointmentController@delete');
    Route::post('/appointment/delete/force', 'AppointmentController@forceDelete');

    Route::get('/schedule/{timestamp?}', 'AppointmentController@schedule');
    
    Route::get('/userlist', 'UserController@userlist');
    Route::post('/user/update', 'UserController@updateUser');
    Route::post('/invite/update', 'InviteController@updateInvite');
    Route::post('/user/remove', 'UserController@removeUser');
    Route::post('/invite/remove', 'InviteController@removeInvite');

    Route::get('/timeslot', 'AppointmentController@timeslot');
    Route::post('/timeslot/create', 'AppointmentController@createTimeslot');
    Route::post('/timeslot/delete', 'AppointmentController@deleteTimeslot');

    Route::get('/emailsetting', 'UserController@emailSetting');
    Route::post('/emailsetting', 'UserController@updateEmailSetting');
    
    Route::get('/calendarsetting', 'UserController@calendarSetting');
    Route::post('/calendarsetting', 'UserController@updateCalendarSetting');

    Route::get('/agencysetting', 'UserController@agencySetting');
    Route::post('/agency/create', 'UserController@createAgency');
    Route::post('/agency/update', 'UserController@updateAgency');
    Route::post('/agency/edit', 'UserController@editAgency');
    Route::post('/agency/remove', 'UserController@removeAgency');

    Route::get('/subscription', 'UserController@subscription');

    Route::get('/subscribe', 'SubscriptionController@index');
    Route::post('/subscribe', 'SubscriptionController@store');

    Route::get('/setting', 'UserController@setting');
});

Route::middleware(['auth', 'client'])->prefix('client')->group(function () {
    Route::get('/calendar/{user_id}/{timestamp?}', 'AppointmentController@bookingCalendar');
    Route::post('/appointment/search/{user_id}', 'AppointmentController@search');
    Route::post('/appointment/create', 'AppointmentController@createByClient');
    Route::post('/appointment/update', 'AppointmentController@update');
    Route::post('/appointment/delete', 'AppointmentController@delete');
});

Route::middleware(['auth', 'client'])->group(function () { 
    Route::get('/calendar/{user_id}', 'AppointmentController@index');
    Route::post('/appointment/search/{user_id}', 'AppointmentController@search');
});

Route::middleware(['auth', 'user'])->group(function () {
    Route::get('/dashboard', 'UserController@index')->name('dashboard');
    
    Route::get('/account', 'UserController@account');
    Route::post('/user/info', 'UserController@updateInfo');  

    Route::get('/email', 'UserController@statistic');
    // Route::post('/statistic', 'UserController@getStatistic');
    Route::post('/email', 'UserController@sendMessage');

    // Route::get('/statistic/{user_id}', 'UserController@statistic');
    // Route::post('/statistic/{user_id}', 'UserController@getStatistic');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/password/change', 'Auth\ChangePasswordController@showChangeForm');
    Route::post('/password/change', 'Auth\ChangePasswordController@change');

    Route::get('/help', 'UserController@help');

    Route::get('/feedback', 'UserController@feedback');
});

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm');
    Route::post('/login', 'Auth\AdminLoginController@login');
    
    Route::get('/register', 'Auth\AdminRegisterController@showRegistrationForm');
});

Route::middleware(['auth', 'admin'])->prefix('admin')->group(function () {
    Route::get('/dashboard', 'AdminController@index');
    Route::get('/', 'AdminController@index');
    
    Route::get('/users', 'AdminController@users')->name('user.list');
    Route::get('/users/{id}', 'AdminController@delete_user')->name('delete.user');
    Route::get('/users/{id}/{verify}', 'AdminController@verify_email')->name('verify.email');
    
    Route::get('/invite', 'AdminController@showInviteForm');
    Route::post('/invite', 'AdminController@invite');
    
    Route::get('/invites', 'AdminController@invites');
});