var addAppointment = function(index) {
    var strongs = document.getElementsByTagName("strong");
    for (var i=0;i<strongs.length;i++) {
      strongs[i].innerHTML = "";
    }
    var elements = document.getElementsByClassName('has-error');      
    for (var i=elements.length - 1;i>=0;i--) {
      elements[i].classList.remove('has-error');
    }
    if (window.axios) {
      const url = '/appointment/create'; 
      var data = new FormData(document.getElementById('appointment-form-' + index));
      axios.post(url, data) 
      .then(function(response) {
        console.log(response.data);
        if (response.data.errors) {
          for(var key in response.data.errors) {
            $('#'+key+'-'+index).html(response.data.errors[key]);
            document.getElementById(key+'_'+index).classList.add('has-error');
          }
        }
        if (response.data.client_id) {
          $('#client_id-'+index).html(response.data.client_id);
          document.getElementById('client_id_'+index).classList.add('has-error');
        }
        if (response.data.start_time) {
          $('#start_time-'+index).html(response.data.start_time);
          document.getElementById('start_time'+index).classList.add('has-error');
        }
        if (response.data.success) {
          window.location = window.location;
        }
      }) 
      .catch(function(error) {
        console.log(error);
      });
    } 
  };

  var updateAppointment = function(index) {
    var strongs = document.getElementsByTagName("strong");
    for (var i=0;i<strongs.length;i++) {
      strongs[i].innerHTML = "";
    }
    var elements = document.getElementsByClassName('has-error');      
    for (var i=elements.length - 1;i>=0;i--) {
      elements[i].classList.remove('has-error');
    }
    if (window.axios) {
      const url = '/appointment/update'; 
      var data = new FormData(document.getElementById('appointment-form-' + index));
      axios.post(url, data) 
      .then(function(response) {
        console.log(response.data);
        if (response.data.errors) {
          for(var key in response.data.errors) {
            $('#'+key+'-'+index).html(response.data.errors[key]);
            document.getElementById(key+'_'+index).classList.add('has-error');
          }
        }
        if (response.data.client_id) {
          $('#client_id-'+index).html(response.data.client_id);
          document.getElementById('client_id_'+index).classList.add('has-error');
        }
        if (response.data.start_time) {
          $('#start_time-'+index).html(response.data.start_time);
          document.getElementById('start_time'+index).classList.add('has-error');
        }
        if (response.data.success) {
          let timeoffset = new Date().getTimezoneOffset();
          var timestamp = response.data.timestamp - timeoffset * 60;
          window.location = '/schedule/' + timestamp;
        }
      }) 
      .catch(function(error) {
        console.log(error);
      });
    } 
  };

  $('select#type').change(function() {
    // console.log($(this).val())
    // console.log($(this).parents('div.row').next().find('select#length'))
    // if ($(this).val() != -1) {
      // $(this).parents('form').find('select#length').prop('disabled', false);
      if ($(this).val() == 0) {
        $(this).parents('form').find('select#length').val(3);
      } else if ($(this).val() == 1) {
        $(this).parents('form').find('select#length').val(1);
      }
      // if ($(this).parents('div.row').next().next().find('select#client_id').val() != -1) {
      //   $(this).parents('form').find('[type=submit]').removeAttr("disabled");
      // }
    // } else {
    //   $(this).parents('div.row').next().find('select#length').prop('disabled', true);
    //   $(this).parents('form').find('[type=submit]').attr("disabled", "disabled");
    // }
  });
  // $('select#length').change(function() {
  //   if ($(this).val() != -1) {
  //     if ($(this).parents('div.row').next().find('select#client_id').val() != -1) {
  //       $(this).parents('form').find('[type=submit]').removeAttr("disabled");
  //     }
  //   } else {
  //     $(this).parents('form').find('[type=submit]').attr("disabled", "disabled");
  //   }
  // });
  console.log(clients);
  $('select#client_id').change(function() {
    console.log('**************************')
    let id = $(this).val();
    console.log(id)
    
    var agency = '';
    var name = '';
    let selectedIdx = $('select#client_id option:selected').index();
    if (selectedIdx == 1){
      agency = advisor_agency;
      $(this).parents('div.row').prev().find('input#agency').val(agency)      
      return;      
    }
    for (let index = 0; index < clients.length; index++) {
      if (clients[index].id == id) {
        agency = clients[index].pivot.agency.name;
        name = clients[index].name;
        break;
      }        
    }
    // if ($(this).val() != -1) {
      $(this).parents('div.row').prev().find('input#agency').val(agency)
      // if ($(this).parents('div.row').prev().prev().find('select#type').val() != -1) {
      //   $(this).parents('form').find('[type=submit]').removeAttr("disabled");
      // }
    // } else {
    //   $(this).parents('form').find('[type=submit]').attr("disabled", "disabled");
    // }
  });
  $('#date-title span').datepicker({ 
    autoclose: true,
    // startDate: 'today',
    todayHighlight: true,
    todayBtn: true
  }).on('changeDate', function(e) {
      // `e` here contains the extra attributes
      let timeoffset = new Date().getTimezoneOffset();
      var today = new Date();
      if (e.date.getDate()==today.getDate()&&e.date.getMonth()==today.getMonth()&&e.date.getFullYear()==today.getFullYear()) {          
        window.location = '/schedule';
      } else {
        var timestamp = e.date.getTime() / 1000 - timeoffset * 60;
        window.location = '/schedule/' + timestamp;
      }
  });
  $('#btn-today').click(function() {
    // current = moment();
    // let mode = $('#day-control .btn-action.active').val()
    // drawChart(mode);
  });
  $('#left-arrow').click(function() {
    // let mode = $('#day-control .btn-action.active').val()
    // current.subtract(1, mode)
    // drawChart(mode);
  });
  $('#right-arrow').click(function() {
    // let mode = $('#day-control .btn-action.active').val()
    // current.add(1, mode)
    // drawChart(mode);
  });
  $('.select_date').datepicker({ 
    autoclose: true,
    // startDate: 'today',
    todayHighlight: true,
    todayBtn: true
  });
  //Timepicker
  $('.start_time').timepicker({ 
    showInputs: false, 
    defaultTime: '08:00 AM', 
    minuteStep: 15, 
    showMeridian: true,
    // maxHours: 24, 
  });
  $('.end_time').timepicker({ 
    showInputs: false, 
    defaultTime: '10:00 AM', 
    minuteStep: 15, 
    showMeridian: true, 
    // maxHours: 24, 
  });
  $('.start_time').timepicker().on('changeTime.timepicker', function(e) {
    console.log(e.time.value)
    var start_time = moment(e.time.value, 'H:mm A');
    var end_time = moment($(this).parents('.panel-body').find('.end_time').val(), 'H:mm A');
    if (start_time >= end_time) {
      $(this).parents('.panel-body').find('.end_time').timepicker('setTime', moment(e.time.value, 'H:mm A').add(1, 'hour').format('H:mm A'))
    }
  })
  $('.end_time').timepicker().on('changeTime.timepicker', function(e) {
    console.log(e.time.value)
    var end_time = moment(e.time.value, 'H:mm A');
    var start_time = moment($(this).parents('.panel-body').find('.start_time').val(), 'H:mm A');
    if (start_time >= end_time) {
      $(this).parents('.panel-body').find('.start_time').timepicker('setTime', moment(e.time.value, 'H:mm A').subtract(1, 'hour').format('H:mm A'))
    }
  })
  $('.appointment-remove').click(function() {   
    let id = $(this).siblings('#id').val();   
    if (layer) layer.confirm("Are you sure delete this Appointment?", {
      title: 'Delete',
      btn: ["Yes", "No"]
    }, function(){
      if (window.axios) {
        const url = '/appointment/delete'; 
        const data = { 
          id: id,
        }; 
        console.log(data)
        axios.post(url, data) 
        .then(function(response) {
          location.reload();
          if (layer && response.data.success) window.layer.msg('Deleted Appointment');
        }) 
        .catch(function(error) {
          console.log(error.response.data)
          if (layer) layer.msg(error.response.data.message);
        });
      } 
    }, function(){
      
    });
  });
  $('.appointment-change').click(function() {
      if (window.axios) {
        const url = '/appointment/change'; 
        let id = $(this).siblings('.appointment-id').val();
        const data = { 
          id: id,
        }; 
        console.log(data)
        axios.post(url, data) 
        .then(function(response) {
          location.reload();
          if (layer && response.data.success) window.layer.msg('Changed Appointment');
        }) 
        .catch(function(error) {
          console.log(error.response.data)
          if (layer) layer.msg(error.response.data.message);
        });
      } 
  });
  $('.appointment-fake').click(function() {
    let timestamp = $(this).parents('.panel-body').find('#timestamp').val()
    let start_time = $(this).parents('.panel-body').find('#start_time').val()
    let end_time = $(this).parents('.panel-body').find('#end_time').val()
    let type = parseInt($(this).parents('.panel-body').find('#type').val()) - 12
    make_time(type, timestamp, start_time, end_time)
  });
  var moveDay = function(type) {
    console.log("type", type)
    var timestamp;
    if (type == 1) {
      timestamp = parseInt(currentTimestamp) - 24 * 60 * 60;
    } else {
      timestamp = parseInt(currentTimestamp) + 24 * 60 * 60;
    }
    window.location = '/schedule/' + timestamp;
  };


  var make_time = function(type, timestamp, start_time, end_time) {
      if (window.axios) {
        const url = '/appointment/create/fake';
        const data = { 
          timestamp: timestamp,
          start_time: start_time,
          end_time: end_time,
          type: type,
        }; 
        console.log(data)
        axios.post(url, data) 
        .then(function(response) {
          location.reload();
          if (layer && response.data.success) window.layer.msg('Success');
        }) 
        .catch(function(error) {
          console.log(error.response.data)
          if (layer) layer.msg(error.response.data.message);
        });
      } 
  }
  $('#unavailable_time').click(function() {
    let timestamp = $(this).parents('#open_time').find('#timestamp').val()
    let start_time = $(this).parents('#open_time').find('#start_time').val()
    let end_time = $(this).parents('#open_time').find('#end_time').val()
    make_time(false, timestamp, start_time, end_time)
  })
  $('#available_time').click(function() {
    let timestamp = $(this).parents('#open_time').find('#timestamp').val()
    let start_time = $(this).parents('#open_time').find('#start_time').val()
    let end_time = $(this).parents('#open_time').find('#end_time').val()
    make_time(true, timestamp, start_time, end_time)
  })
  
  // setTimeout(function() {
  //   var ele = document.getElementsByClassName("in")[0].parentElement;
  //   console.log(ele)
  //   // ele.scrollIntoView({behavior: 'smooth'});
  //   var top = $('.in').position().top;
  //   console.log(top)
  //   // window.scrollTo(0, top - 120)
  //   // window.scroll(0, top - 120);
  //   window.scroll({
  //     top: top - 120,
  //     left: 0,
  //     behavior: 'smooth'
  //   })
  // }, 300);
  if ($('.in').get(0)) {
    $('html, body').animate({
      // scrollTop: $('.in').offset().top - 220
      scrollTop: $('.in').position().top - 120
    }, 500, function() {});
  }

var calendar_setting = "{{$calendar_setting}}";
var mode = 'year';
var defaultView = 'listYear';
if (calendar_setting == 1) {
  defaultView = 'agendaDay';
  mode = 'day';
  console.log("clicked day")
} else if (calendar_setting == 2) {
  defaultView = 'agendaWeek';
  mode = 'week';
} else if (calendar_setting == 3) {
  defaultView = 'month';
  mode = 'month';
}