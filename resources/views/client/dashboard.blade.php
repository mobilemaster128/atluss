@extends('template')

@section('title_postfix', 'Dashboard')

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form clearfix">
        <div class="heading text-center">Advisors List</div>
        <ol class="list-group">
          @forelse($users as $user)
          <li style="margin: 16px 0;">
            <a href="javascript:void(0)" class="" style="">{{ $user->name }}</a>
            <a href="{{ url('email/' . $user->id) }}" class="btn btn-action pull-right text-blue" style="margin: 0; padding: 2px 12px;">Statistics</a>
            <a href="{{ url('client/calendar/' . $user->id) }}" class="btn btn-action pull-right text-blue" style="margin: 0 16px; padding: 2px 12px;">Calendar</a>
          </li>
          @empty
          @endforelse
        </ol>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop