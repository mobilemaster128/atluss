@extends('template')

@section('title_postfix', 'Account')

@section('css')
<style>
  .card-js input,
  .card-js input:focus {
    background-color: #e1e1e1;
    color: #353535;
    border: 1px solid #ccd0d2;
  }
  .card-js .expiry-container,
  .card-js .cvc-container {
    width: 100%;
    margin: 0 0 16px;
  }
  .card-js .expiry-container .expiry-wrapper,
  .card-js .cvc-container .cvc-wrapper {
    margin: 0;
  }
</style>
@stop

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form">
        {{--  <a class="btn btn-action btn-back" style="position: absolute;" href="{{ url('dashboard') }}"><span class="fc-icon fc-icon-left-single-arrow"></span></a>  --}}
        <div class="heading text-center">Account Information</div>
        @if (session('error'))
        <div class="alert alert-danger">
          {{ session('error') }}
        </div>
        @endif
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-warning">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-detail" aria-expanded="false"
                  aria-controls="collapse-detail">
                  User details
                </a>
              </h4>
            </div>
            <div id="collapse-detail" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingone">
              <div class="panel-body">
                <form action="{{ url('user/info') }}" method="post" id="detail-form">
                {!! csrf_field() !!}
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                  <input type="text" class="form-control" placeholder="User Name" id="name" name="name"
                  @if (old('name') !== null)
                  value="{{ old('name') }}" 
                  @elseif ($user['name'] !== null)
                  value="{{ $user['name'] }}" 
                  @endif
                  required>
                  @if ($errors->has('name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                  <input type="text" class="form-control" placeholder="Company Name" id="company" name="company"
                  @if (old('company') !== null)
                  value="{{ old('company') }}" 
                  @elseif ($user['company'] !== null)
                  value="{{ $user['company'] }}" 
                  @endif
                  required>
                  @if ($errors->has('company'))
                  <span class="help-block">
                    <strong>{{ $errors->first('company') }}</strong>
                  </span>
                  @endif
                </div>
                <button type="submit" class="btn btn-action">Submit</button>
                @if (session('status'))
                <div class="alert alert-success">
                  {{ session('status') }}
                </div>
                @endif
              </form>
              </div>
            </div>
          </div>
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-warning">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-password" aria-expanded="false"
                    aria-controls="collapse-password">
                    Change Password
                  </a>
                </h4>
              </div>
              <div id="collapse-password" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <form action="{{ url('password/change') }}" method="post" id="password-form">
                    {!! csrf_field() !!}
                    <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                      <input type="text" class="form-control" placeholder="Current password" id="current_password" name="current_password" required>
                      @if ($errors->has('current_password'))
                      <span class="help-block">
                        <strong>{{ $errors->first('current_password') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                      <input type="text" class="form-control" placeholder="Password" id="password" name="password" required>
                      @if ($errors->has('password'))
                      <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                      <input type="text" class="form-control" placeholder="Confirm password" id="password_confirmation" name="password_confirmation" required>
                      @if ($errors->has('password_confirmation'))
                      <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                      </span>
                      @endif
                    </div>
                    <button type="submit" class="btn btn-action">Change</button>
                    @if (session('success'))
                    <div class="alert alert-success">
                      {{ session('success') }}
                    </div>
                    @endif
                  </form>
                </div>
              </div>
            </div>
            @if (Auth::user()->isAdvisor())
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-warning">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-payment" aria-expanded="false"
                      aria-controls="collapse-payment">
                      Payment Information
                    </a>
                  </h4>
                </div>
                <div id="collapse-payment" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                  <div class="panel-body">
                    <form action="{{ url('user/profile/update') }}" method="post" id="update-form">
                      {!! csrf_field() !!}
                      <div class="stripe-errors"></div>
                      <div class="row">
                        <div class="col-sm-6">
                          <h3 class="text-center">Credit Card Info</h3>
                          {{-- <div class="expiry_value" @if (old('expire_month') !== null && old('expire_year') !== null)
                          value="{{ old('expire_month') }}/{{ old('expire_year') }}" 
                          @elseif ($profile['expiry_value'] !== null)
                          value="{{ $profile['expiry_value'] }}" 
                          @endif style="display: none;"></div> --}}
                          {{-- <div class="card-js">
                            <input class="card-number my-custom-class" name="card_number" id="card_number" @if (old('card_number') !== null)
                            value="{{ old('card_number') }}" 
                            @elseif ($profile['card_number'] !== null)
                            value="{{ $profile['card_number'] }}" 
                            @endif
                            required>
                            <input class="name" name="card_name" id="card_name" @if (old('card_name') !== null)
                            value="{{ old('card_name') }}" 
                            @elseif ($profile['card_name'] !== null)
                            value="{{ $profile['card_name'] }}" 
                            @endif
                            required>
                            <input class="expiry-month" id="expiration-date" name="expire_month">
                            <input class="expiry-year" id="expiration-date" name="expire_year">
                            <input class="cvc" name="cvc" id="cvc" @if (old('cvc') !== null)
                            value="{{ old('cvc') }}" 
                            @elseif ($profile['cvc'] !== null)
                            value="{{ $profile['cvc'] }}" 
                            @endif
                            required>
                          </div> --}}

                          <div class="form-group {{ $errors->has('card_name') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Card Name" id="card_name" name="card_name"
                            @if (old('card_name') !== null)
                            value="{{ old('card_name') }}" 
                            @elseif ($profile['card_name'] !== null)
                            value="{{ $profile['card_name'] }}" 
                            @endif
                            required>
                            @if ($errors->has('card_name'))
                            <span class="help-block">
                              <strong>{{ $errors->first('card_name') }}</strong>
                            </span>
                            @endif
                          </div>
                          <div class="form-group {{ $errors->has('card_number') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Name on Card" id="card_number" name="card_number"
                            @if (old('card_number') !== null)
                            value="{{ old('card_number') }}" 
                            @elseif ($profile['card_number'] !== null)
                            value="{{ $profile['card_number'] }}" 
                            @endif
                            required>
                            @if ($errors->has('card_number'))
                            <span class="help-block">
                              <strong>{{ $errors->first('card_number') }}</strong>
                            </span>
                            @endif
                          </div>
                          <div class="form-group {{ $errors->has('expires_at') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="MM/YY" id="expires_at" name="expires_at"
                            @if (old('expires_at') !== null)
                            value="{{ old('expires_at') }}" 
                            @elseif ($profile['expires_at'] !== null)
                            value="{{ $profile['expires_at'] }}" 
                            @endif
                            required>
                            @if ($errors->has('expires_at'))
                            <span class="help-block">
                              <strong>{{ $errors->first('expires_at') }}</strong>
                            </span>
                            @endif
                          </div>
                          <div class="form-group {{ $errors->has('cvc') ? 'has-error' : '' }}">
                            <input type="text" maxlength="3" class="form-control" placeholder="CVC" id="cvc" name="cvc"
                            @if (old('cvc') !== null)
                            value="{{ old('cvc') }}" 
                            @elseif ($profile['cvc'] !== null)
                            value="{{ $profile['cvc'] }}" 
                            @endif
                            required>
                            @if ($errors->has('cvc'))
                            <span class="help-block">
                              <strong>{{ $errors->first('cvc') }}</strong>
                            </span>
                            @endif
                          </div>
                        </div>
                        <div class="col-sm-6 ">
                          <h3 class="text-center">Billing Address</h3>
                          <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Name" id="user_name" name="user_name"
                            @if (old('user_name') !== null)
                            value="{{ old('user_name') }}" 
                            @elseif ($profile['user_name'] !== null)
                            value="{{ $profile['user_name'] }}" 
                            @endif
                            required>
                            @if ($errors->has('user_name'))
                            <span class="help-block">
                              <strong>{{ $errors->first('user_name') }}</strong>
                            </span>
                            @endif
                          </div>
                          <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Address" id="address" name="address"
                            @if (old('address') !== null)
                            value="{{ old('address') }}" 
                            @elseif ($profile['address'] !== null)
                            value="{{ $profile['address'] }}" 
                            @endif
                            required>
                            @if ($errors->has('address'))
                            <span class="help-block">
                              <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                          </div>
                          <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="City" id="city" name="city"
                            @if (old('city') !== null)
                            value="{{ old('city') }}" 
                            @elseif ($profile['city'] !== null)
                            value="{{ $profile['city'] }}" 
                            @endif
                            required>
                            @if ($errors->has('city'))
                            <span class="help-block">
                              <strong>{{ $errors->first('city') }}</strong>
                            </span>
                            @endif
                          </div>
                          <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                            <select class="form-control" id="state" name="state" required>
                              @foreach ($states as $state)
                              <option value="{{ $state->id }}"
                                @if (old('state') == $state->id) selected 
                                @elseif ($profile['state'] == $state->name) selected 
                                @endif>{{ $state->name }}</option>
                              @endforeach
                            </select>
                            @if ($errors->has('state'))
                            <span class="help-block">
                              <strong>{{ $errors->first('state') }}</strong>
                            </span>
                            @endif
                          </div>
                          <div class="form-group {{ $errors->has('zip_code') ? 'has-error' : '' }}">
                            <input type="text" maxlength="5" class="form-control" placeholder="Zip Code" id="zip_code" name="zip_code"
                            @if (old('zip_code') !== null)
                            value="{{ old('zip_code') }}" 
                            @elseif ($profile['zip_code'] !== null)
                            value="{{ $profile['zip_code'] }}" 
                            @endif
                            required>
                            @if ($errors->has('zip_code'))
                            <span class="help-block">
                              <strong>{{ $errors->first('zip_code') }}</strong>
                            </span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <button type="button" onClick="updateSub()" class="btn btn-action">Update</button>
                      @if (session('status'))
                      <div class="alert alert-success">
                        {{ session('status') }}
                      </div>
                      @endif
                    </form>
                  </div>
                </div>
              </div>
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-warning">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-subscription" aria-expanded="false" aria-controls="collapse-subscription">
                        Cancel Subscription
                      </a>
                    </h4>
                  </div>
                  <div id="collapse-subscription" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                    <form method="POST" action="{{ url('user/profile/cancel') }}" id="cancel-form">
                      {!! csrf_field() !!}
                      <div class="panel-body">
                        <button type="submit" class="btn btn-action">Cancel Subscription</button>
                      </div>
                    </form>
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop

@push('js')
<script src="https://js.stripe.com/v2/"></script>
<script>
  // $('#expires_at').datepicker({ 
  //     autoclose: true,
  //     startDate: '+1d'
  // });
  // var im = new Inputmask("99/99");
  // im.mask(document.getElementById('expires_at'))
  // $('#expires_at').inputmask({regex: '[01-12]'});
  $('#expires_at').mask("99/99");
  let expire = $('.expiry_value').attr('value');
  // console.log($('input.expiry'));
  // console.log(expire);
  // console.log('******************************');
  // $('input.expiry').value('expire');

  Stripe.setPublishableKey('{{ config('services.stripe.key') }}');
  function updateSub() {
    var form = $('#update-form');
    var card_number = $('#card_number').val();
    var expire_data = $('#expires_at').val();
    var exps = expire_data.split('/');
    var cvc = $('#cvc').val();

    if (!Stripe.card.validateCardNumber(card_number)) {
      form.find('.stripe-errors').text('Card Number is Invalid').addClass('alert alert-danger');
      return;
    } else if (!Stripe.card.validateCVC(cvc)) {
      form.find('.stripe-errors').text('CVC is Invalid').addClass('alert alert-danger');
      return;
    } else if (!Stripe.card.validateExpiry(exps[0], exps[1])) {
      form.find('.stripe-errors').text('Card is expire').addClass('alert alert-danger');
      return;
    }
    // disable the form button
    form.find('button').prop('disabled', true);

    // use the stripe library. create a single use token
    Stripe.card.createToken({
      number:card_number,
      cvc:cvc,
      exp_month:exps[0],
      exp_year:exps[1],
      }, function(status, response) {

      // if there are errors, show them
      if (response.error) {
        form.find('.stripe-errors').text('Invalid Card Infomation').addClass('alert alert-danger');
        form.find('button').prop('disabled', false);
      } else {

        // if there are no errors...
        console.log(response);

        // append the token to the form
        form.append($('<input type="hidden" name="stripeToken">').val(response.id));

        // submit the form
        form.get(0).submit();
      }
    });
  }
</script>
@endpush