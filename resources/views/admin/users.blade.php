@extends('admin.page') 

@section('css')
@stop 

@section('content_header')
<h1>{{ trans('menu.user_list') }}</h1>
@stop 

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class=box>
            <div class="box-header text-center">
                <h3 class="box-title">{{ trans('menu.user_list') }}</h3>
            </div>
            <input type="hidden" id="first_index" name="first_index" value="{{ $users->firstItem() ?: 1 }}" >
            {{--  <input type="hidden" id="first_index" name="first_index" value="1" >  --}}
            <div class="box-body">
                <table id="userlist" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Company</th>
                            {{--  <th>type</th>  --}}
                            <th>Card Name</th>
                            <th>Card Number</th>
                            {{--  <th>Expires At</th>  --}}
                            {{--  <th>CVC</th>  --}}
                            <th>User Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip Code</th>
                            <th>Clients</th>
                            <th>Created At</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($users as $user)
                        <tr>
                            <td></td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->company }}</td>
                            {{--  <td>@if ($user->type==2) Advisor @else Client @endif</td>  --}}
                            <td>{{ $user->profile['card_name'] }}</td>
                            <td>{{ $user->profile['card_number'] }}</td>
                            {{--  <td>{{ $user->profile['expires_at'] }}</td>  --}}
                            {{--  <td>{{ $user->profile['cvc'] }}</td>  --}}
                            <td>{{ $user->profile['user_name'] }}</td>
                            <td>{{ $user->profile['address'] }}</td>
                            <td>{{ $user->profile['city'] }}</td>
                            <td>{{ $user->profile['state'] }}</td>
                            <td>{{ $user->profile['zip_code'] }}</td>
                            <td>{{ $user->clients }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td><select id="verify{{$user->id}}" onchange="verify_email({{$user->id}});">
                                <option value="1" @if($user->confirmed == 1) selected="selected" @endif>Verified</option>
                                <option value="0" @if($user->confirmed == 0) selected="selected" @endif>Not Verified</option>
                            </select></td>
                            <td><a href="{{route('delete.user', $user->id)}}">Delete</a></td>
                        </tr>
                        @empty @endforelse
                    </tbody>
                </table>
                <div class="text-center">
                {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop 

@section('js')
<script>
    var table = $('#userlist').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : false,
        'autoWidth'   : false,
        'scrollX'     : true,
        'scrollY'     : false,
        'scrollCollapse':false,
        'columnDefs': [ {
            'searchable': false,
            'orderable': false,
            'targets': [0],
        } ],
        'order': [[ 1, 'asc' ]]
    });

    var firstItem = parseInt($("#first_index").val());
 
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+firstItem;
        } );
    } ).draw();

    function verify_email(id) {
        var verify = document.getElementById("verify" + id).value;
        window.location.href = window.location.origin + "/admin/users/" + id + "/" + verify;
    }
</script>
@stop