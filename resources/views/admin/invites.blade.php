@extends('admin.page') 

@section('css')
@stop 

@section('content_header')
<h1>{{ trans('menu.invite_manage') }}</h1>
@stop 

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class=box>
            <div class="box-header text-center">
                <h3 class="box-title">{{ trans('menu.invite_manage') }}</h3>
            </div>
            <input type="hidden" id="first_index" name="first_index" value="{{ $invites->firstItem() ?: 1 }}" >
            {{--  <input type="hidden" id="first_index" name="first_index" value="1" >  --}}
            <div class="box-body">
                <table id="userlist" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Email</th>
                            <th>From</th>
                            <th>Type</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($invites as $invite)
                        <tr>
                            <td></td>
                            <td>{{ $invite->email }}</td>
                            <td>{{ $invite->user->name }}</td>
                            <td>@if ($invite->user->isAdmin()) Advisor @else User @endif</td>
                            <td>{{ $invite->created_at }}</td>
                        </tr>
                        @empty @endforelse
                    </tbody>
                </table>
                <div class="text-center">
                {{ $invites->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop 

@section('js')
<script>
    var table = $('#userlist').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : false,
        'autoWidth'   : false,
        'scrollX'     : true,
        'scrollY'     : false,
        'scrollCollapse':false,
        'columnDefs': [ {
            'searchable': false,
            'orderable': false,
            'targets': [0],
        } ],
        'order': [[ 1, 'asc' ]]
    });

    var firstItem = parseInt($("#first_index").val());
 
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+firstItem;
        } );
    } ).draw();
</script>
@stop