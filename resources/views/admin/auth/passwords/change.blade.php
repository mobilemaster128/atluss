@extends('admin.page')

@section('content_header')
@stop 

@section('content')
<div class="row">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ trans('passwords.change_password') }}</p>
            <form action="{{ url(config('adminlte.password_reset_url', 'password/change')) }}" method="post">
                {!! csrf_field() !!}
                <div class="form-group has-feedback {{ $errors->has('current_password') ? 'has-error' : '' }}">
                    <input type="password" name="current_password" class="form-control" value="" placeholder="{{ trans('passwords.current_password') }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span> @if ($errors->has('current_password'))
                    <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span> @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control" placeholder="{{ trans('adminlte::adminlte.password') }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span> @if ($errors->has('password'))
                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span> @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span> @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span> @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('passwords.change_password') }}</button>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
</div>
@stop