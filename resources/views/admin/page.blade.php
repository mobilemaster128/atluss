@extends('adminlte::page')

@section('title', config('app.name'))

@push('css')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Style -->
<link rel="stylesheet" href="{{ asset('css/admin.css') }}">
<link rel="stylesheet" href="{{ asset('css/admin-style.css') }}">
<link rel="stylesheet" href="{{ asset('css/admin-bower.css') }}">
@endpush

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>You are logged in!</p>
@stop

@push('js')
<!-- Scripts -->
<script src="{{ asset('js/admin.js') }}"></script>
<script src="{{ asset('js/admin-bower.js') }}"></script>
@endpush