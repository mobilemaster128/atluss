@extends('basic.master') 
{{--  @extends('adminlte::master')  --}}

@section('adminlte_css')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('css/app-style.css') }}">
<link rel="stylesheet" href="{{ asset('css/bower.css') }}">
<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
<style>
.flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
}
</style>
@stack('css') 
@yield('css') 
@stop 

@section('body_class', '') 

@section('body')
<nav id="header" class="navbar">
    <div id="header-container" class="container navbar-container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="brand" class="navbar-brand" href="{{ url('home') }}">
                <img src="{{ asset('img/logo.png') }}" alt="logo"/>
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse navbar-right">
            <ul id="user-contact">
                @auth
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                @endauth
                @guest
                <li>
                    <a href="{{ url('register') }}">Sign Up</a>
                </li>
                <li>
                    <a href="{{ url('login') }}">Log in</a>
                </li>
                @endguest
            </ul>
            <ul class="list-inline social-links">
                <li>
                    <a href="" class="icon-facebook"></a>
                </li>
                <li>
                    <a href="" class="icon-twitter"></a>
                </li>
                <li>
                    <a href="" class="icon-google"></a>
                </li>
                <li>
                    <a href="" class="icon-instagram"></a>
                </li>
            </ul>
            <br>
            <ul class="nav navbar-nav pull-right" style="text-align: center;">
                <li @isset($home) class="active" @endisset>
                    <a @guest href="{{ url('home') }}" @endguest
                    @auth
                    href="{{ url('dashboard') }}"
                    @endauth>Home</a>
                </li>
                <li @isset($about) class="active" @endisset>
                    <a href="{{ url('about') }}">About</a>
                </li>
                <li @isset($faq) class="active" @endisset>
                    <a href="{{ url('faq') }}">Faq </a>
                </li>
                <li @isset($contactus) class="active" @endisset>
                    <a href="{{ url('contactus') }}">Contact us </a>
                </li>
                <li @isset($pricing) class="active" @endisset>
                    <a href="{{ url('pricing') }}">Pricing</a>
                </li>
                @auth
                <li class="visible-xs">
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                @endauth
                @guest
                <li class="visible-xs">
                    <a href="{{ url('register') }}">Sign Up</a>
                </li>
                <li class="visible-xs">
                    <a href="{{ url('login') }}">Log in</a>
                </li>
                @endguest
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->
<div class="wrapper">
    <div class="header-atluss">
        @yield('header')  
    </div>   
    <div class="banner-atluss">
        @yield('banner')
    </div>
    <div class="content-atluss">
        @yield('content') 
    </div>
    <div class="feature-atluss">
        @yield('feature')
    </div>
</div>
<!-- ./wrapper -->
<div class="footer-btm">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <a id="brand" class="footer-brand" href="{{ url('home') }}">
                    <img src="{{ asset('img/footer-logo.png') }}" alt="logo"/>
                </a>
            </div>
            <div class="col-xs-12 col-sm-9">
                <ul class="list-inline">
                    <li>                        
                        <a href="{{ url('home') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ url('about') }}">About</a>
                    </li>
                    <li>
                        <a href="{{ url('faq') }}">Faq </a>
                    </li>
                    <li>
                        <a href="{{ url('contactus') }}">Contact us </a>
                    </li>
                    <li>
                        <a href="{{ url('pricing') }}">Pricing</a>
                    </li>
                </ul>
                @auth
                <a class="btn round btn-login" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();" style="padding: 6px 24px;">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                @endauth
                @guest
                <a class="btn round btn-login" href="{{ route('login') }}" style="margin-right: 24px;padding: 6px 24px;">
                    Log in
                </a>
                <a class="btn round btn-login" href="{{ route('register') }}" style="padding: 6px 24px;">
                    Sign up
                </a>
                @endguest
            </div>
        </div>
    </div>
</div>
@stop 

@section('adminlte_js')
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bower.js') }}"></script>
<script>
    $(function() {
        var myNavBar = {

            flagAdd: true,

            elements: [],

            init: function (elements) {
                this.elements = elements;
            },

            add : function() {
                if(this.flagAdd) {
                    for(var i=0; i < this.elements.length; i++) {
                        // document.getElementById(this.elements[i]).className += " fixed-theme";
                        $(this.elements[i]).addClass('fixed-theme');
                        if (i == 0) $(this.elements[i]).addClass('navbar-fixed-top');
                    }
                    // document.getElementsByTagName("BODY")[0].className += " fixed-theme";
                    this.flagAdd = false;
                }
            },

            remove: function() {
                for(var i=0; i < this.elements.length; i++) {
                    // document.getElementById(this.elements[i]).className =
                    //         document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
                    $(this.elements[i]).removeClass('fixed-theme');
                    $(this.elements[i]).removeClass('navbar-fixed-top');
                }
                // document.getElementsByTagName("BODY")[0].className =
                //             document.getElementsByTagName("BODY")[0].className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
                this.flagAdd = true;
            }

        };

        /**
         * Init the object. Pass the object the array of elements
         * that we want to change when the scroll goes down
         */
        myNavBar.init(  [
            "#header",
            "#header-container",
            "#brand"
        ]);

        /**
         * Function that manage the direction
         * of the scroll
         */
        function offSetManager(){

            var yOffset = 100;
            var currYOffSet = window.pageYOffset;

            if(yOffset < currYOffSet) {
                myNavBar.add();
            }
            else if(currYOffSet == 0){//yOffset){
                myNavBar.remove();
            }

        }

        /**
         * bind to the document scroll detection
         */
        window.onscroll = function(e) {
            offSetManager();
        }

        /**
         * We have to do a first detectation of offset because the page
         * could be load with scroll down set.
         */
        offSetManager();
    })
</script>
@stack('js')
@yield('js') 
@stop