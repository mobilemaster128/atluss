@extends('template')

@section('title_postfix', 'Dashboard')
@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Welcome</div>
</div>
@stop

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form">
        <div class="heading text-center">Dashboard</div>
        <ul class="dash_listing list-inline">
          <li>
            <a href="{{ url('schedule') }}">
              <h3>My Schedule</h3>
              <i class="fa fa-calendar"></i>
            </a>
          </li>
          <li>
            <a href="{{ url('userlist') }}">
              <h3>User List</h3>
              <i class="fa fa-users"></i>
            </a>
          </li>
          <li>
            <a href="{{ url('setting') }}">
              <h3>Setting</h3>
              <i class="fa fa-cog"></i>
            </a>
          </li>
          <li>
            <a href="{{ url('email') }}">
              <h3>Email</h3>
              <i class="fa fa-envelope "></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop