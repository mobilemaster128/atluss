@extends('basic.page')

@section('title_postfix', 'About')

@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Pricing</div>
</div>
@stop

@section('banner')
<div class="container text-center">
  <p>Sign up today and receive a proven appointment scheduling system. Atluss is a virtual connection toolkit that is exceedingly powerful, yet easy to use. With Atluss you share your calendar through a personal URL, allowing your partner agencies and staff (users) to schedule appointments in real time, based on your preferences and ever-changing availability. Atluss will foster an environment where partner agencies will want to compete for your time. Atluss gives you complete control to manage and organize your schedule in the way that keeps you working at your peak productivity.</p>
</div>
@stop

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-5 col-md-offset-1">
        <div class="pricing-frame">
          <div class="heading text-center" style="background: #00cced; color: white; margin: 0px; padding: 20px 0px;border-radius: 10px 10px 0 0;">Monthly</div>
          <div class="pricing_content ">
            <p>For only $50.00 per month you will find yourself working at your peak productivity! Atluss will fix your scheduling challenges with its exceedingly powerful, yet easy to use virtual connection toolkit. Join Atluss today and you’ll wonder how you ever managed without it.</p>
          </div>
          <div class="text-center">
            <div class="rate">$50.00</div>
            @guest
            <a class="btn btn-black round btn-login" href="{{ route('register') }}">Sign up >></a>
            @endguest
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-5">
        <div class="pricing-frame">
            <div class="heading text-center" style="background: #00cced; color: white; margin: 0px; padding: 20px 0px;border-radius: 10px 10px 0 0;">Yearly</div>
          <div class="pricing_content">
            <p>For only $550.00 annually you will find yourself working at your peak productivity! Atluss will fix your scheduling challenges with its exceedingly powerful, yet easy to use virtual connection toolkit. Join Atluss today and you’ll wonder how you ever managed without it.</p>
          </div>
          <div class="text-center">
            <div class="rate">$550.00</div>
            @guest
            <a class="btn btn-black round btn-login" href="{{ route('register') }}">Sign up >></a>
            @endguest
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@guest
<div class="container">
    <div class="feature text-center">
        <div class="container">
            <h3>Sign Up Now to Get started!</h3>
            <a class="btn round btn-login" href="{{ route('register') }}">Sign up >></a>
        </div>
    </div>
</div>
@endguest
@auth
<div class="container">
    <div class="feature text-center">
        <div class="container">
            <h3>Now to Get started!</h3>
            <a class="btn round btn-login" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>
        </div>
    </div>
</div>
@endauth
@stop