@extends('basic.page')

@section('title_postfix', 'About')

@section('header')
<div class="container-fluid text-center">
    <div class="header_text">About Atluss</div>
</div>
@stop

@section('banner')
<div class="container text-center">
  <p>
    Hall of Fame Personal Financial Representative, Willie Herring, has avoided no-shows, double-booked appointments and unnecessary long rides from one appointment to the next for the past 18 years.
    How?
    He and his team implemented a manual scheduling system so simple, yet so powerful, that success was at his fingertips. However, it still allowed for human error and the occasional headaches…until now – The creation of Atluss. Atluss takes this proven system for scheduling appointments and turns it into an automated tool – a Virtual Connection Toolkit – that promises to improve your productivity, efficiency and organization while always remaining top of mind with your agency partners. Atluss will foster an environment where partner agencies will want to compete for your time.
  </p>
</div>
@stop

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-5 dash-text">
        <h2>DASHBOARD</h2>
        <p>See a snapshot of your upcoming appointments. See which users are setting appointments and what types are being set. Monitor which users are setting productive appointments and which ones may need a little coaching. You determine what information you need to know immediately. Your dashboard is your window into how well your financial services network is producing for you.
        </p>
      </div>
      <div class="col-sm-6 col-md-7 dash-img">
        <img src="{{ asset('/img/desktop.png') }}" class="img-responsive" alt="destop-img">
      </div>
    </div>
  </div>
</section>
<section class="section-atluss">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-7 schedule-img">
        <img src="{{ asset('/img/schedule.png') }}" class="img-responsive" alt="destop-img">
      </div>
      <div class="col-sm-6 col-md-5 schedule-text">
        <h2>SCHEDULES</h2>
        <p>Once your users have set an appointment based on your preferences and your ever-changing availability, you’ll receive an email filled with imperative meeting information and your updated schedule will be automatically emailed to each of your users.
        </p>
      </div>
    </div>
  </div>
</section>
<section class="section-atluss">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 referral-text">
        <h2>REFERRALS</h2>
        <p>When a new prospect or existing customer calls one of your partner agencies, that’s when Atluss takes over. The user opens your calendar, reviews your availability and right then, with the customer in front of them, books them on your schedule, complete with all the necessary information – name, address, phone, reason for appointment, etc. Instantly that slot is filled and you and your other users are notified. Block off your vacation. Immediately make your users aware that you’re taking the afternoon off. You control your calendar. No double-booking, no inefficient travel, no wasted time.
        </p>
      </div>
      <div class="col-sm-6 referral-img">
        <img src="{{ asset('/img/referral.png') }}" class="img-responsive" alt="destop-img">
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@guest
<div class="container">
    <div class="feature text-center">
        <div class="container">
            <h3>Sign Up Now to Get started!</h3>
            <a class="btn round btn-login" href="{{ route('register') }}">Sign up >></a>
        </div>
    </div>
</div>
@endguest
@auth
<div class="container">
    <div class="feature text-center">
        <div class="container">
            <h3>Now to Get started!</h3>
            <a class="btn round btn-login" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>
        </div>
    </div>
</div>
@endauth
@stop