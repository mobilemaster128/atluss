@extends('template')

@section('title_postfix', 'Statistic')

@push('css')
<style>
</style>
@endpush

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <form  action="{{ url('email') }}" method="post" id="message-form" class="login-form">
        @csrf
        {{-- <a class="btn btn-action btn-back" style="position: absolute;" href="{{ url('dashboard') }}"><span class="fc-icon fc-icon-left-single-arrow"></span></a>
        <div class="heading text-center">
          @isset($user) {{ $user->name }}'s  @endisset Statistics<br>
          <div class="text-center btn-group" id="day-control">
            <button type="button" class="btn btn-action active" value="day">Day</button>
            <button type="button" class="btn btn-action" value="week">Week</button>
            <button type="button" class="btn btn-action" value="month">Month</button>
            <button type="button" class="btn btn-action" value="year">Year</button>
          </div>
          <br>
          <button type="button" class="btn btn-action pull-right" id="btn-today" style="margin-left: 16px;">today</button>
          <div class="text-center btn-group pull-right" id="arrow-group">
            <button type="button" class="btn btn-action btn-back" id="left-arrow"><span class="fc-icon fc-icon-left-single-arrow"></span></button>
            <button type="button" class="btn btn-action btn-back" id="right-arrow"><span class="fc-icon fc-icon-right-single-arrow"></span></button>
          </div>
          <div class="pull-left">
            <h3 id="date-title">{{ date('l F j,Y') }}</h3>
          </div>          
          <input type="hidden" id="user_id" name="user_id" @isset($user) value="{{ $user->id }}" @endisset />          
        </div>
        <!-- BAR CHART -->
        <div class="chart">
          <canvas id="barChart" style="height:230px"></canvas>
        </div> --}}
        <div class="row">      
          <div class="col-md-6">
            <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
              <input type="text" class="form-control" placeholder="Subject" id="subject" name="subject" value="{{ old('subject') }}" required>
              @if ($errors->has('subject'))
              <span class="help-block">
                <strong>{{ $errors->first('subject') }}</strong>
              </span> @endif
            </div>
            <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
              <textarea class="form-control" placeholder="Message" form="message-form" id="message" name="message" rows="5" required>@if(old('message')!==null){{ old('message') }}@endif</textarea>
              @if ($errors->has('message'))
              <span class="help-block">
                <strong>{{ $errors->first('message') }}</strong>
              </span> @endif
            </div>
          </div>
          <div class="col-md-6 form-group text-center">
              <button type="submit" class="btn btn-black round btn-login" href="">Send Message</button>
          </div>
        </div>
        @if (session('status'))
        <div class="alert alert-success">
          Your message has been sent
        </div>
        @endif

        
        <ol class="list-group">
          @forelse ($users as $user)
          <li>{{ $user->name }}</li>
          @empty
          @endforelse
        </ol>
      </form>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop

@push('js')
<script>
    let backgrounds = [ 'rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 
                'rgba(75, 192, 192, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(255, 159, 64, 0.2)' ];
    let borders = [ 'rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 
                'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)' ];
    var current = moment();
    var ctx = $("#barChart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        options: {
          title: {
            display: false,
            position: 'top',
            text: 'Appointment Chart',
            fontColor: '#333',
            fontSize: 24
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
                // max: 100,
                min: 0,
                // stepSize: 20,
                suggestedMax: 5,
                maxTicksLimit: 6
              }
            }]
          },
          legend: {
            display: false,
            position: 'top',
            labels: {
              fontColor: 'rgb(255, 99, 132)'
            }
          },
          tooltips: {
            enabled: true
          }
        }
    });
    let resetChart = function() {
      myChart.data.labels = [];//.pop();
      myChart.data.datasets = [{              
              label: [],
              data: [],
              backgroundColor: [],
              borderColor: [],
              borderWidth: 1
            }];
      myChart.update();
    }
    let loadData = function(start, end) {
      // if (window.axios) {
      //   const user_id = $('#user_id').val();
      //   const url = user_id ? '/statistic/' + user_id : '/statistic'; 
      //   let timeoffset = new Date().getTimezoneOffset();
      //   const data = { 
      //     start: start.unix(),//format(), 
      //     end: end.unix(),//format(),
      //     // timezone: timeoffset,
      //   };
      //   console.log(data)
      //   axios.post(url, data) 
      //   .then(function(response) {
      //     console.log(response.data)
      //     if (response.data.success) {
      //       resetChart();
      //       let result = response.data.statistics;
      //       var max = 0;
      //       for (let index in result) {
      //         let statistic = result[index];
      //         myChart.data.labels.push(statistic.name);
      //         // myChart.data.datasets.forEach((dataset) => {
      //         //   dataset.data.push(statistic.appointments.length);
      //         //   dataset.backgroundColor.push(backgrounds[index % 6]);
      //         //   dataset.borderColor.push(borders[index % 6]);
      //         // });
      //         $.each(myChart.data.datasets, function(key, dataset) {
      //           dataset.data.push(statistic.appointments.length);
      //           dataset.backgroundColor.push(backgrounds[index % 6]);
      //           dataset.borderColor.push(borders[index % 6]);
      //         });
      //         // myChart.data.datasets.forEach((dataset) => {
      //         //   dataset.label = 'Statistics on Appointment';
      //         // });
      //         $.each(myChart.data.datasets, function(key, dataset) {
      //           dataset.label = 'Statistics on Appointment';
      //         });
      //         max = max > statistic.appointments.length ? max : statistic.appointments.length;
      //       }
      //       if (max >= 10) {
      //         myChart.options.scales.yAxes[0].ticks.maxTicksLimit = 11
      //       }
      //       myChart.update();
      //     }
      //   }) 
      //   .catch(function(error) {
      //     console.log(error.response.data)
      //     if (layer) layer.msg(error.response.data.message);
      //   });
      // }
    }
    let drawChart = function(mode) {
      switch(mode) {
        case 'day':
          $('#date-title').text(current.format('dddd MMMM D,GGGG'));
          // $('#date-title').text(moment().format('dddd MMMM D,GGGG'));
          loadData(current.clone().startOf('day'), current.clone().endOf('day'));
        break;
        case 'week':
          $('#date-title').text(current.clone().startOf('week').format('MMMM D ~ ') + current.clone().endOf('week').format('MMMM D') + moment().format(' GGGG'));
          loadData(current.clone().startOf('week'), current.clone().endOf('week'));
        break;
        case 'month':
          $('#date-title').text(current.format('MMMM GGGG'));
          loadData(current.clone().startOf('month'), current.clone().endOf('month'));
        break;
        case 'year':
          $('#date-title').text(current.format('GGGG'));
          loadData(current.clone().startOf('year'), current.clone().endOf('year'));
        break;
      }
    }
    $('#btn-today').click(function() {
      current = moment();
      let mode = $('#day-control .btn-action.active').val()
      drawChart(mode);
    });
    $('#left-arrow').click(function() {
      let mode = $('#day-control .btn-action.active').val()
      current.subtract(1, mode)
      drawChart(mode);
    });
    $('#right-arrow').click(function() {
      let mode = $('#day-control .btn-action.active').val()
      console.log(mode)
      console.log(current)
      current.add(1, mode)
      console.log(current)
      drawChart(mode);
    });
    $('#day-control .btn-action').click(function() {
      $(this).siblings('.btn-action').removeClass('active');
      $(this).addClass('active');
      drawChart($(this).val());
    })
    drawChart('day');
</script>
@endpush