@extends('basic.page')

@section('banner')
<div class="container text-center">
  <ul class="list-inline">
    @if (Auth::user()->isAdvisor())
    <li>
      <a href="{{ url('calendar') }}">My Calendar</a>
    </li>
    <li>
      <a href="{{ url('dashboard') }}">Dashboard</a>
    </li>
    @elseif (Auth::user()->isClient())
    <li>
      <a href="{{ url('dashboard') }}">Advisors List</a>
    </li>
    @endif
    <li>
      <a href="{{ url('account') }}">View Account</a>
    </li>
    <li>
      <a href="{{ url('contactus') }}">Help</a>
    </li>
    <li>
      <a href="{{ url('feedback') }}">Leave Feedback</a>
    </li>
    <li>
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
  </ul>
</div>
@stop