<form action="{{ url('contactus') }}" method="post" id="contact-form" class="login-form">
  {!! csrf_field() !!}
  <div class="heading text-center">Send Us a Message</div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group {{ $errors->has('full_name') ? 'has-error' : '' }}">
        <input type="text" class="form-control" placeholder="Full Name" id="full_name" name="full_name" value="{{ old('full_name') }}" required>
        @if ($errors->has('full_name'))
        <span class="help-block">
          <strong>{{ $errors->first('full_name') }}</strong>
        </span> @endif
      </div>
      <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
        <input type="email" class="form-control" placeholder="Email" id="email" name="email" value="{{ old('email') }}" required>
        @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span> @endif
      </div>
      <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
        <input type="tel" class="form-control" placeholder="Phone" id="phone_number" name="phone_number" value="{{ old('phone_number') }}" required>
        @if ($errors->has('phone_number'))
        <span class="help-block">
          <strong>{{ $errors->first('phone_number') }}</strong>
        </span> @endif
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
        <textarea class="form-control" placeholder="Message" form="contact-form" id="message" name="message" rows="5" required>@if(old('message')!==null){{ old('message') }}@endif</textarea>
        @if ($errors->has('message'))
        <span class="help-block">
          <strong>{{ $errors->first('message') }}</strong>
        </span> @endif
      </div>
    </div>
  </div>
  <div class="row">
    <div class="flex-center">
      <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
        {!! Recaptcha::render([ 'lang' => 'en' ]) !!}
        @if ($errors->has('g-recaptcha-response'))
        <span class="help-block">
            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
        </span>
        @endif
      </div>
    </div>
  </div>
  <div class="form-group text-center">
      <button type="submit" class="btn btn-black round btn-login" href="">Send us >></button>
  </div>
  @if (session('status'))
  <div class="alert alert-success">
    {{ session('status') }}
  </div>
  @endif
</form>