@extends('template')

@section('title_postfix', 'Help')

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form">
        <div class="heading text-center">Help</div>
        <p>1. Authentication  Email + Password account registration and authentication 
           Password not retained but stored as one-way hash 
           Email based account recovery via unique token. 
           Two account types (Advisor, Client) 
           User invitation with pre-populated user data. 
          
          2. Payment Gateway Integration  Stripe account setup & verification 
           Implement subscription form, models, and endpoints 
           Stripe event webhook endpoints (i.e. payment declined) 
           User payment mailers (receipt, notice, welcome) 
          
          3. Calendar  Allow users to view availability and directs users to the booking process via click/drag event on an available time slot. 
           Allow user to drag to select a custom time slot or click a preset timeslot based on advisor’s settings 
           Depending on browser support requirements may be able to take advantage of HTML5 API 
          
          4. Booking Process  Event booking form & form validation 
           Display event details, status, recent changes 
           Event driven invitation mailer 
           Timed update mailer (8:30am default update via settings) 
          
          5. Account Settings  Allow user to manage account settings 
           Update password, email settings, billing, cancel account… 
           Advisor user types also have advanced calendar booking settings and email subject/body defaults 
          
          
          
          6. Non business logic front-end development  Building layout, structure, and copy in HTML+CSS 
           CSS library based on the designs to be inherited across the website and html based emails to ensure a consistent user experience 
           Static pages (FAQ, home page, contact page…) 
          
          7. Administrative Portal  Interface for the client to manage user accounts and perform administrative tasks 
           List users, their status (invited - advisor - user), most recent sign on, billing flags, and other applicable details. 
           Modify user type, blacklist users, issue warning. 
           Modify certain sections of website copy (FAQ, contact info...) 
          
          8. Initial Deployment  Configure server(s) and deploy application 
           Configure resource monitoring and error logging services 
           Configure domain/DNS 
           Configure SSL (security for user information) 
           Configure CDN (improves speed/performance) 
           Benchmark performance 
          </p>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop