@extends('errors.page')

@section('content')
<h1>{{ $exception->getMessage() }}</h1>

<p>
	Perhaps you would like to go to our <a href="{{ URL::route('home') }}">home page</a>?
</p>
@stop
