@extends('template')

@section('title_postfix', 'Invite') 

@section('content')
<section class="section-atluss">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="pricing-frame">
                    <form action="{{ url('user/invite') }}" method="post" id="invite-form" class="login-form">
                        {!! csrf_field() !!}
                        <a class="btn btn-action btn-back" style="position: absolute;" href="{{ url('userlist') }}"><span class="fc-icon fc-icon-left-single-arrow"></span></a>
                        <div class="heading text-center">Invite User</div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email" class="control-label">User Email</label>
                            <input type="email" class="form-control" placeholder="Email" id="email" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('agency') ? 'has-error' : '' }}">
                            <label for="agency" class="control-label">Agency Name</label>
                            <select class="form-control" id="agency" name="agency" required>
                              @foreach($agencies as $agency)
                              <option value="{{ $agency->id }}" @if(old('agency')!=NULL && old('agency')==$agency->id) selected @endif>{{ $agency->name }}</option>
                              @endforeach
                            </select>
                            @if ($errors->has('agency'))
                            <span class="help-block">
                                <strong>{{ $errors->first('agency') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                            {!! Recaptcha::render([ 'lang' => 'en' ]) !!}
                            @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-grou text-center">
                            <button type="submit" class="btn btn-black round btn-login">Invite >></button>
                        </div>
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                        @if (session('danger'))
                        <div class="alert alert-danger">
                            {!! session('danger') !!}
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 

@push('js')
<script>
$('#agency').editableSelect({ filter: false });
</script>
@endpush 