@extends('template')

@section('title_postfix', 'Setting')

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form">
        <a class="btn btn-action btn-back" style="position: absolute;" href="{{ url('dashboard') }}"><span class="fc-icon fc-icon-left-single-arrow"></span></a>
        <div class="heading text-center clearfix">Settings</div>
        <ul class="list-group">
          <li><a href="{{ url('timeslot') }}" class="list-group-item" style="border: none;">Timeslot</a></li>
          <li><a href="{{ url('emailsetting') }}" class="list-group-item" style="border: none;">Email Setting</a></li>
          <li><a href="{{ url('agencysetting') }}" class="list-group-item" style="border: none;">Agency Setting</a></li>
          <li><a href="{{ url('calendarsetting') }}" class="list-group-item" style="border: none;">Calendar Setting</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop

@section('js')
@stop