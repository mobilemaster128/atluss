@extends('template')

@section('title_postfix', 'Userlist')

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form clearfix">
        {{--  <a class="btn btn-action btn-back" style="position: absolute;" href="{{ url('dashboard') }}"><span class="fc-icon fc-icon-left-single-arrow"></span></a>  --}}
        <div class="heading text-center">Users List</div>
        <div class="clearfix">
          <a class="btn btn-action" href="{{ url('user/invite') }}">Add New Users</a>
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          @forelse($users as $user)
          <div class="panel panel-warning">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $loop->index }}" aria-expanded="false"
                  aria-controls="collapse-{{ $loop->index }}">
                  {{ $user->name }} : {{ $user->pivot->agency->name }}
                </a>
              </h4>
            </div>
            <div id="collapse-{{ $loop->index }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingone">
              <div class="panel-body">
                <form id="user-form-{{ $loop->index }}" action="{{ url('user/update') }}" method="POST">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <select class="form-control" id="agency" name="agency" required>
                      @foreach($agencies as $agency)
                      <option value="{{ $agency->id }}" @if ($user->pivot->agency_id == $agency->id) selected @endif>{{ $agency->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <input type="hidden" id="id" name="id" value="{{ $user->id }}"/>
                  <button type="submit" class="btn btn-action" style="margin-right: 16px;">Update</button>
                  <button type="button" class="btn btn-action remove-button text-red" value="user">Remove</button>
                </form>
              </div>
            </div>
          </div>
          @empty
          @endforelse
          @forelse($invites as $invite)
          <div class="panel panel-warning">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a class="collapsed text-red" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $invite->id }}" aria-expanded="false"
                  aria-controls="collapse{{ $invite->id }}">
                  {{ $invite->email }} : {{ $invite->agency->name }}
                </a>
              </h4>
            </div>
            <div id="collapse{{ $invite->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingone">
              <div class="panel-body">
                <form id="invite-form-{{ $invite->id }}" action="{{ url('invite/update') }}" method="POST">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <select class="form-control" id="agency" name="agency" required>
                      @foreach($agencies as $agency)
                      <option value="{{ $agency->id }}" @if ($invite->agency_id == $agency->id) selected @endif>{{ $agency->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <input type="hidden" id="id" name="id" value="{{ $invite->id }}"/>
                  <button type="button" class="btn btn-action resend-button" style="margin-right: 16px;">Resend</button>
                  <button type="submit" class="btn btn-action" style="margin-right: 16px;">Update</button>
                  <button type="button" class="btn btn-action remove-button text-red" value="invite">Remove</button>
                </form>
              </div>
            </div>
          </div>
          @empty
          @endforelse
        </div>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop

@section('js')
<script>
  $('.resend-button').click(function() {    
    if (window.axios) {
      const url = '/invite/resend'; 
      let id = $(this).siblings('#id').val();
      const data = { 
        id: id,
      }; 
      console.log(data)
      axios.post(url, data) 
      .then(function(response) {
        // location.reload();
        if (layer && response.data.success) window.layer.msg('Sent Invitation again');
      }) 
      .catch(function(error) {
        console.log(error.response.data)
        if (layer) layer.msg(error.response.data.message);
      });
    } 
  })
  $('.remove-button').click(function() { 
    var url = '';
    if ($(this).val() == 'invite') {
      url = '/invite/remove'; 
    } else {
      url = '/user/remove'; 
    }
    let self = $(this);
    if (layer) layer.confirm("Are you sure delete this User?", {
      title: 'Delete',
      btn: ["Yes", "No"]
    }, function(){
      if (window.axios) {
        let id = self.siblings('#id').val();
        const data = { 
          id: id,
        }; 
        console.log(data)
        axios.post(url, data) 
        .then(function(response) {
          // location.reload();
          if (layer && response.data.success) window.layer.msg('Removed');
          self.parents('.panel').remove();
        }) 
        .catch(function(error) {
          console.log(error.response.data)
          if (layer) layer.msg(error.response.data.message);
        });
      } 
    }, function(){
      
    });
  })
</script>
@stop