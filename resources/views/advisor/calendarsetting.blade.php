@extends('template')

@section('title_postfix', 'Calendar')

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <form action="{{ url('calendarsetting') }}" method="post" id="calendar-form" class="login-form">
        <a class="btn btn-action btn-back" style="position: absolute;" href="{{ url('dashboard') }}"><</a>
        <div class="heading text-center clearfix">Calendar Settings</div>
        {!! csrf_field() !!}
        <div class="row">
          <input type="radio" class="form-control" id="day" name="calendar" value="1" {{$calendar_setting == 1 ? 'checked' : ''}}><label for="day"> Day</label>
          <input type="radio" class="form-control" id="week" name="calendar" value="2" {{$calendar_setting == 2 ? 'checked' : ''}}><label for="week"> Week</label>
          <input type="radio" class="form-control" id="month" name="calendar" value="3" {{$calendar_setting == 3 ? 'checked' : ''}}><label for="month"> Month</label>
          <input type="radio" class="form-control" id="year" name="calendar" value="4" {{$calendar_setting == 4 ? 'checked' : ''}}><label for="year"> Year</label>
          <input type="submit" class="btn btn-action form-control" style="width:40%; margin: 30px auto 0;display:block;" value="Save">
        </div>
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
      </form>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop

@section('js')
@stop