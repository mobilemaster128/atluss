@extends('template')

@section('title_postfix', 'Email')

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <form action="{{ url('emailsetting') }}" method="post" id="email-form" class="login-form">
        <a class="btn btn-action btn-back" style="position: absolute;" href="{{ url('dashboard') }}"><</a>
        <div class="heading text-center clearfix">Email Settings</div>
        <input type="hidden" id="timezone" name="timezone">
        {!! csrf_field() !!}
        <div class="row">
          <div class="col-md-6">
              <div class="form-group {{ $errors->has('daily_content') ? 'has-error' : '' }}">
                <label for="daily_content" class="control-label" style="margin-top:16px;"><h4>Daily Email Content</h4></label>
                  <textarea class="form-control" tabindex=1 rows="5" id="daily_content" placeholder="Daily Email Content..." name="daily_content" form="email-form"
                    >@if(old('daily_content')!==null){{ old('daily_content') }}@elseif ($daily_content !== null){{ $daily_content }}@endif</textarea> 
                  @if ($errors->has('daily_content'))
                  <span class="help-block">
                    <strong>{{ $errors->first('daily_content') }}</strong>
                  </span>
                  @endif
              </div>
              <div class="form-group row {{ $errors->has('daily_time') ? 'has-error' : '' }}">
                <label for="daily_time" class="col-sm-12 control-label"><h4>Daily Email Time</h4></label>
                <div class="col-sm-12">
                  <input type="text" tabindex=3 class="form-control" id="daily_time" placeholder="08:00 AM" name="daily_time"
                  @if (old('daily_time') !== null)
                  value="{{ old('daily_time') }}" 
                  @elseif ($daily_time !== null)
                  value="{{ $daily_time }}"
                  @endif >
                  @if ($errors->has('daily_time'))
                  <span class="help-block">
                    <strong>{{ $errors->first('daily_time') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group {{ $errors->has('reminder_content') ? 'has-error' : '' }}">
                <label for="reminder_content" class="control-label" style="margin-top:16px;"><h4>Reminder Email Content</h4></label>
                  <textarea class="form-control" tabindex=2 rows="5" id="reminder_content" placeholder="Reminder Email Content..." name="reminder_content" form="email-form"
                    >@if(old('reminder_content')!==null){{ old('reminder_content') }}@elseif ($reminder_content !== null){{ $reminder_content }}@endif</textarea> 
                  @if ($errors->has('reminder_content'))
                  <span class="help-block">
                    <strong>{{ $errors->first('reminder_content') }}</strong>
                  </span>
                  @endif
              </div>
              <div class="form-group row {{ $errors->has('prior_time') ? 'has-error' : '' }}">
                <label for="prior_time" class="col-sm-12 control-label"><h4>Reminder Prior Hour</h4></label>
                <div class="col-sm-12">
                  <input type="number" tabindex=4 step="24" min="24" max="120" class="form-control" id="prior_time" placeholder="24" name="prior_time"
                  @if (old('prior_time') !== null)
                  value="{{ old('prior_time') }}" 
                  @elseif ($prior_time !== null)
                  value="{{ $prior_time }}"
                  @endif >
                  @if ($errors->has('prior_time'))
                  <span class="help-block">
                    <strong>{{ $errors->first('prior_time') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
          </div>
        </div>
        <div class="form-group {{ $errors->has('appointment_content') ? 'has-error' : '' }}">
          <label for="appointment_content" class="control-label" style="margin-top:16px;"><h4>New Appointment Email Content</h4></label>
            <textarea class="form-control" tabindex=5 rows="5" id="appointment_content" placeholder="New Appointment Email Content..." name="appointment_content" form="email-form"
              >@if(old('appointment_content')!==null){{ old('appointment_content') }}@elseif ($appointment_content !== null){{ $appointment_content }}@endif</textarea> 
            @if ($errors->has('appointment_content'))
            <span class="help-block">
              <strong>{{ $errors->first('appointment_content') }}</strong>
            </span>
            @endif
        </div>
        <div class="" style="margin:32px 0px;">
          <button class="btn btn-action" type="submit" style="margin-right:32px;">Save</button>
          <button class="btn btn-action" type="button" id="sendnow">Send now</button>
        </div>
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif 
      </form>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop

@section('js')
<script>
  let timeoffset = new Date().getTimezoneOffset();
  $('#timezone').val(timeoffset)
  $('#daily_time').timepicker({ 
    showInputs: false,
    defaultTime: '08:00 AM',
    minuteStep: 1,//30,
    showMeridian: true,
  });
  $('#sendnow').click(function() {
    if (window.axios) {
      let daily_content = $('#daily_content').text();
      let daily_time = $('#daily_time').val();
      let appointment_content = $('#appointment_content').val();
      let reminder_content = $('#reminder_content').val();
      let prior_time = $('#prior_time').val();
      const url = '/emailsetting'; 
      const data = {
        daily_content: daily_content,
        daily_time: daily_time,
        timezone: timeoffset,
        appointment_content: appointment_content,
        reminder_content: reminder_content,
        prior_time: prior_time,
        send_now: true
      };
      console.log(data)
      axios.post(url, data) 
      .then(function(response) {
        console.log(response.data)
        if (layer && response.data.success) {
          layer.msg('Successfully sent');
        }
      }) 
      .catch(function(error) {
        console.log(error.response.data)
        if (layer) layer.msg(error.response.data.message);
      });
    }
  })
</script>
@stop