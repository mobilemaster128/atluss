@extends('template')

@section('title_postfix', 'Schedule')

@push('css')
<style>
  #arrow-group .btn-action {
    min-width: 0;
  }
  .btn-action span {
    font-size: 1em;
    vertical-align: middle;
  }
  textarea#description {
    min-height: 196px;
  }
</style>
@endpush

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form">
        <a class="btn btn-action btn-back" style="position: absolute;" onClick="moveDay(1)"><span class="fc-icon fc-icon-left-single-arrow"></span></a>
        <a class="btn btn-action btn-back" style="position: relative; float:right;" onClick="moveDay(2)"><span class="fc-icon fc-icon-right-single-arrow"></span></a>        
        
        <div class="heading text-center clearfix">
          My Schedule
          <br>
          <h3 class="text-center" id="date-title">{{ $title->format('l F j, Y') }}<span class="glyphicon glyphicon-chevron-down" style="margin: 4px 0 0 4px; font-size: 16px;"></span></h3>
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          @forelse ($appointments as $appointment)
          <div class="panel panel-warning">
            @if (empty($appointment->client_id))
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title @if ($appointment->type==10 || $appointment->type==12) text-purple @elseif ($appointment->type==11 || $appointment->type==13) text-red @endif">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $loop->index }}" aria-expanded="false"
                  aria-controls="collapse-{{ $loop->index }}">
                  {{ $appointment->start->format('g:i A') . ' ~ ' . $appointment->end->format('g:i A') }}
                  <span>{{ $appointment->title }}</span>
                </a>
              </h4>
            </div>
            <div id="collapse-{{ $loop->index }}" class="panel-collapse collapse @if ((old('active_id') === NULL && $appointment->start->format('U') == $timeStamp) || (old('active_id') != NULL && $loop->index == old('active_id'))) in @endif" role="tabpanel" aria-labelledby="headingTwo">
              @if ($appointment->type==10 || $appointment->type==11)
              <div class="panel-body">
                <input type="hidden" class="appointment-id" name="id" id="id" value="{{ $appointment->id }}">                 
                <div id="create{{ $loop->index }}" class="collapse">
                  <form action="#" method="post" id="appointment-form-{{ $loop->index }}" class="">
                    {!! csrf_field() !!}
                    <input type="hidden" name="active_id" id="active_id"  value="{{ $loop->index }}">
                    <input type="hidden" class="form-control text" id="appointment_date" name="appointment_date" value="{{ $title->format('m/d/Y') }}" >
                    <div class="row">
                      <div class="col-sm-6">
                        <div id="start_time_{{ $loop->index }}" class="form-group">
                          <label>Appointment Time</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" tabindex=1 class="form-control start_time" id="start_time" placeholder="08:00" name="start_time"
                            @if (old('start_time') !== null)
                            value="{{ old('start_time') }}"
                            @else
                            value="{{ $appointment->start->format('g:i A') }}"
                            @endif required>
                          </div>
                          <span class="help-block">
                            <strong id="start_time-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div class="form-group">
                          <label>Agency Name</label>
                          <input type="text" tabindex=3 class="form-control" id="agency" name="agency" placeholder="Agency Name" value="" readonly required>
                          <span class="help-block">
                            <strong id="setby-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div id="type_{{ $loop->index }}" class="form-group">
                          <label>Appointment Type</label>
                          <select tabindex=2 class="form-control" id="type" name="type" required>
                            <option value="">Select Appointment Type</option>
                            @foreach($types as $type)
                            <option value="{{ $loop->index }}" @if(old('type')!=NULL && old('type')==$loop->index) selected @endif>{{ $type }}</option>
                            @endforeach
                          </select>
                          <span class="help-block">
                            <strong id="type-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="length_{{ $loop->index }}" class="form-group">
                          <label>Length of Meeting</label>
                          <select tabindex=4 class="form-control" id="length" name="length" required>
                            <option value="">Select Meeting Length</option>
                            @foreach($lengths as $length)
                            <option value="{{ $loop->index }}" @if(old('length')!=NULL && old('length')==$loop->index) selected @endif>{{ $length }}</option>
                            @endforeach
                          </select>
                          <span class="help-block">
                            <strong id="length-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div id="setby_{{ $loop->index }}" class="form-group">
                          <label>Appointment Set By:</label>
                          <input type="text" tabindex=5 class="form-control" id="setby" name="setby" placeholder="Name" value="{{ Auth::user()->name }}" readonly required>
                          <span class="help-block">
                            <strong id="setby-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div id="client_id_{{ $loop->index }}" class="form-group">
                          <label>User Name</label>
                          <select tabindex=6 class="form-control" id="client_id" name="client_id" required>
                            <option value="">Select User Name</option>
                            <option value="{{ Auth::user()->id }}">{{ Auth::user()->name }}</option>
                            @foreach($clients as $client)
                            <option value="{{ $client->id }}" @if(old('client_id')!=NULL && old('client_id')==$client->id) selected @endif>{{ $client->name }}</option>
                            @endforeach
                          </select>
                          {{-- <input type="text" class="form-control" id="client_id" name="client_id" placeholder="Name" value="{{ old('client_id') }}" required> --}}
                          <span class="help-block">
                            <strong id="client_id-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div id="name_{{ $loop->index }}" class="form-group">
                          <label>Contact Name</label>
                          <input type="text" tabindex=7 class="form-control" id="name" name="name" placeholder="Contact Name" value="{{ old('name') }}" required>
                          <span class="help-block">
                            <strong id="name-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="address_{{ $loop->index }}" class="form-group">
                          <label>Contact Address</label>
                          <input type="text" tabindex=9 class="form-control" id="address" name="address" placeholder="Address" value="{{ old('address') }}" required>
                          <span class="help-block">
                            <strong id="address-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div id="state_{{ $loop->index }}" class="form-group">
                          <label>Contact State</label>
                          <select tabindex=8 class="form-control" id="state" name="state" required>
                            @foreach ($states as $state)
                            <option value="{{ $state->id }}"
                              @if (old('state') == $state->id) selected 
                              @endif>{{ $state->name }}</option>
                            @endforeach
                          </select>
                          <span class="help-block">
                            <strong id="state-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="zip_code_{{ $loop->index }}" class="form-group">
                          <label>Contact Zip code</label>
                          <input type="tel" tabindex=10 class="form-control" id="zip_code" name="zip_code" placeholder="Zip code" value="{{ old('zip_code') }}" required>
                          <span class="help-block">
                            <strong id="zip_code-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div id="city_{{ $loop->index }}" class="form-group">
                          <label>Contact City</label>
                          <input type="text" tabindex=11 class="form-control" id="city" name="city" placeholder="City" value="{{ old('city') }}" required>
                          <span class="help-block">
                            <strong id="city-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="phone_number_{{ $loop->index }}" class="form-group">
                          <label>Contact Phone Number</label>
                          <input type="tel" tabindex=12 class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number" value="{{ old('phone_number') }}" required>
                          <span class="help-block">
                            <strong id="phone_number-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="email_{{ $loop->index }}" class="form-group">
                          <label>Contact Email</label>
                          <input type="email" tabindex=13 class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                          <span class="help-block">
                            <strong id="email-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div id="description_{{ $loop->index }}" class="form-group">
                          <label>Reason for Appointment/Details</label>
                          <textarea class="form-control" tabindex=14 rows="8" id="description" placeholder="Reason for Appointment/Details..." name="description" form="appointment-form-{{ $loop->index }}"
                            required>@if(old('description')!==null){{ old('description') }}@endif</textarea> 
                          <span class="help-block">
                            <strong id="description-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="">
                      <button type="button" class="btn btn-action text-black" onClick="addAppointment({{$loop->index}})">Add Appointment</button>
                    </div>
                  </form>
                </div>
                <button type="button" class="btn btn-action
                @if ($appointment->type == 10) text-red @elseif ($appointment->type == 11) text-purple @endif appointment-change">
                @if ($appointment->type == 10) Make Unavailable
                @elseif ($appointment->type == 11) Make Available
                @endif
                </button>
                @if ($appointment->type == 10) 
                <button type="button" class="btn btn-action text-purple" data-toggle="collapse" href="#create{{ $loop->index }}" style="margin-left: 16px;">Create Appointment</button>            
                @endif
                <button type="button" class="btn btn-action appointment-remove" style="margin-left: 16px;">Remove</button>  
              </div>
              @elseif ($appointment->type==12 || $appointment->type==13)
              <div class="panel-body">  
                <input type="hidden" name="timestamp" id="timestamp" value="{{ $title->getTimestamp() }}" > 
                <input type="hidden" name="active_id" id="active_id" value="{{ $loop->index }}">
                <input type="hidden" name="type" id="type" value="{{ $appointment->type }}">
                <div class="input-group">
                  <div class="col-xs-6">
                    <div class="row">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control start_time" id="start_time" placeholder="08:00 AM" name="start_time" value="{{ $appointment->start->format('g:i A') }}">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="row">
                      <div class="input-group">
                        <span class="input-group-addon">~</span>
                        <input type="text" class="form-control end_time" id="end_time" placeholder="10:00 AM" name="end_time" value="{{ $appointment->end->format('g:i A') }}">
                      </div>
                    </div>
                  </div>
                </div>
                <div id="create{{ $loop->index }}" class="collapse" style="padding-top: 16px;">
                  <form action="#" method="post" id="appointment-form-{{ $loop->index }}" class="">
                    {!! csrf_field() !!}
                    <input type="hidden" name="active_id" id="active_id"  value="{{ $loop->index }}">
                    <input type="hidden" class="form-control text" id="appointment_date" name="appointment_date" value="{{ $title->format('m/d/Y') }}" >
                    <div class="row">
                      <div class="col-sm-6">
                        <div id="start_time_{{ $loop->index }}" class="form-group">
                          <label>Appointment Time</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control start_time" id="start_time" placeholder="08:00" name="start_time"
                            @if (old('start_time') !== null)
                            value="{{ old('start_time') }}"
                            @else
                            value="{{ $appointment->start->format('g:i A') }}"
                            @endif required>
                          </div>
                          <span class="help-block">
                            <strong id="start_time-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div class="form-group">
                          <label>Agency Name</label>
                          <input type="text" class="form-control" id="agency" name="agency" placeholder="Agency Name" value="" readonly required>
                          <span class="help-block">
                            <strong id="setby-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div id="type_{{ $loop->index }}" class="form-group">
                          <label>Appointment Type</label>
                          <select class="form-control" id="type" name="type" required>
                            <option value="">Select Appointment Type</option>
                            @foreach($types as $type)
                            <option value="{{ $loop->index }}" @if(old('type')!=NULL && old('type')==$loop->index) selected @endif>{{ $type }}</option>
                            @endforeach
                          </select>
                          <span class="help-block">
                            <strong id="type-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="length_{{ $loop->index }}" class="form-group">
                          <label>Length of Meeting</label>
                          <select class="form-control" id="length" name="length" required>
                            <option value="">Select Meeting Length</option>
                            @foreach($lengths as $length)
                            <option value="{{ $loop->index }}" @if(old('length')!=NULL && old('length')==$loop->index) selected @endif>{{ $length }}</option>
                            @endforeach
                          </select>
                          <span class="help-block">
                            <strong id="length-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div id="setby_{{ $loop->index }}" class="form-group">
                          <label>Appointment Set By:</label>
                          <input type="text" class="form-control" id="setby" name="setby" placeholder="Name" value="{{ Auth::user()->name }}" readonly required>
                          <span class="help-block">
                            <strong id="setby-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div id="client_id_{{ $loop->index }}" class="form-group">
                          <label>User Name</label>
                          <select class="form-control" id="client_id" name="client_id" required>                            
                            <option value="">Select User Name</option>
                            <option value="{{ Auth::user()->id }}">{{ Auth::user()->name }}</option>
                            @foreach($clients as $client)
                            <option value="{{ $client->id }}" @if(old('client_id')!=NULL && old('client_id')==$client->id) selected @endif>{{ $client->name }}</option>
                            @endforeach
                          </select>
                          {{-- <input type="text" class="form-control" id="client_id" name="client_id" placeholder="Name" value="{{ old('client_id') }}" required> --}}
                          <span class="help-block">
                            <strong id="client_id-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div id="name_{{ $loop->index }}" class="form-group">
                          <label>Contact Name</label>
                          <input type="text" class="form-control" id="name" name="name" placeholder="Contact Name" value="{{ old('name') }}" required>
                          <span class="help-block">
                            <strong id="name-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="address_{{ $loop->index }}" class="form-group">
                          <label>Contact Address</label>
                          <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{ old('address') }}" required>
                          <span class="help-block">
                            <strong id="address-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div id="state_{{ $loop->index }}" class="form-group">
                          <label>Contact State</label>
                          <select class="form-control" id="state" name="state" required>
                            @foreach ($states as $state)
                            <option value="{{ $state->id }}"
                              @if (old('state') == $state->id) selected 
                              @endif>{{ $state->name }}</option>
                            @endforeach
                          </select>
                          <span class="help-block">
                            <strong id="state-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="zip_code_{{ $loop->index }}" class="form-group">
                          <label>Contact Zip code</label>
                          <input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Zip code" value="{{ old('zip_code') }}" required>
                          <span class="help-block">
                            <strong id="zip_code-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div id="city_{{ $loop->index }}" class="form-group">
                          <label>Contact City</label>
                          <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{ old('city') }}" required>
                          <span class="help-block">
                            <strong id="city-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="phone_number_{{ $loop->index }}" class="form-group">
                          <label>Contact Phone Number</label>
                          <input type="tel" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number" value="{{ old('phone_number') }}" required>
                          <span class="help-block">
                            <strong id="phone_number-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                        <div id="email_{{ $loop->index }}" class="form-group">
                          <label>Contact Email</label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                          <span class="help-block">
                            <strong id="email-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div id="description_{{ $loop->index }}" class="form-group">
                          <label>Reason for Appointment/Details</label>
                          <textarea class="form-control" rows="8" id="description" placeholder="Reason for Appointment/Details..." name="description" form="appointment-form-{{ $loop->index }}"
                            required>@if(old('description')!==null){{ old('description') }}@endif</textarea> 
                          <span class="help-block">
                            <strong id="description-{{ $loop->index }}"></strong>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="">
                      <button type="submit" class="btn btn-action text-black" onClick="addAppointment({{$loop->index}})">Add Appointment</button>
                    </div>
                  </form>
                </div>
                <div style="margin: 16px 0px;">
                  <button type="button" class="btn btn-action appointment-fake
                    @if ($appointment->type == 12) text-red
                    @elseif ($appointment->type == 13) text-purple @endif">
                    @if ($appointment->type == 12) Make Time Unavailable
                    @elseif ($appointment->type == 13) Make Time Available
                    @endif
                    @if ($appointment->type == 12) 
                    <button type="button" class="btn btn-action text-purple" data-toggle="collapse" href="#create{{ $loop->index }}" style="margin-left: 16px;">Create Appointment</button>            
                    @endif
                  </button>
                </div>
              </div>
              @endif
            </div>
            @else
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title @if ($appointment->status) @if ($appointment->type == 0) text-black @else text-teal @endif @else text-red @endif">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $loop->index }}" aria-expanded="true"
                  aria-controls="collapse-{{ $loop->index }}">
                  {{ $appointment->start->format('g:i A') . ' ~ ' . $appointment->end->format('g:i A') }}
                  <span>{{ $appointment->title }}</span>
                </a>
              </h4>
            </div>
            <div id="collapse-{{ $loop->index }}" class="panel-collapse collapse @if ((old('active_id') === NULL && $appointment->start->format('U') == $timeStamp) || (old('active_id') != NULL && $loop->index == old('active_id'))) in @endif" role="tabpanel" aria-labelledby="headingOne">
              <form id="appointment-form-{{$loop->index}}" class="panel-body" action="#" method="post"> 
                {!! csrf_field() !!} 
                <input type="hidden" name="active_id" id="active_id"  value="{{ $loop->index }}">
                <input type="hidden" class="appointment-id" name="id" id="id" value="{{ $appointment->id }}">   
                <h4>Details</h4>
                {{ $appointment->description }}
                <br/>
                <div id="detail{{ $loop->index }}" class="collapse @if ((old('active_id') === NULL && $appointment->start->format('U') == $timeStamp) || (old('active_id') != NULL && $loop->index == old('active_id'))) in @endif" style="margin-top:16px;">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Agency name</label>
                        <p class="form-control">{{ (Auth::user()->clients()->find($appointment->client_id) != null) ? Auth::user()->clients()->find($appointment->client_id)->pivot->agency->name : 'Agency is deleted' }}</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div id="select_date_{{ $loop->index }}" class="form-group">
                        <label>Appointment Date</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control select_date" id="select_date" placeholder="2019-01-01" name="select_date" value="{{ $title->format('m/d/Y') }}">
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div id="start_time_{{ $loop->index }}" class="form-group">
                        <label>Appointment Time</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control start_time" id="start_time" placeholder="08:00 AM" name="start_time"
                          @if (old('start_time')) value="{{ old('start_time') }}"
                          @else value="{{ $appointment->start->format('g:i A') }}"
                          @endif>
                        </div>
                        <span class="help-block">
                          <strong id="start-time-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                  </div> 
                  <div class="row">
                    <div class="col-sm-6">
                      <div id="type_{{ $loop->index }}" class="form-group">
                        <label>Appointment Type</label>
                        <select class="form-control" id="type" name="type" required>
                          <option value="">Select Appointment Type</option>
                          @foreach($types as $type)
                          <option value="{{ $loop->index }}" 
                              @if(old('type')!=NULL && old('type')==$loop->index) selected
                              @elseif ($appointment->type !== null && $appointment->type==$loop->index) selected
                              @endif>{{ $type }}</option>
                          @endforeach
                        </select>
                        <span class="help-block">
                          <strong id="type-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div id="length_{{ $loop->index }}" class="form-group">
                        <label>Length of Meeting</label>
                        <select class="form-control" id="length" name="length" required>
                          <option value="">Select Meeting Length</option>
                          @foreach($lengths as $length)
                          <option value="{{ $loop->index }}" 
                              @if(old('length')!=NULL && old('length')==$loop->index) selected 
                              @elseif ($appointment->length !== null && $appointment->length==$loop->index) selected
                              @endif>{{ $length }}</option>
                          @endforeach
                        </select>
                        <span class="help-block">
                          <strong id="length-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div id="setby_{{ $loop->index }}" class="form-group">
                        <label>Appointment Set By:</label>
                        {{--  <p class="form-control">{{ $appointment->setby }}</p>  --}}
                        <input type="text" class="form-control" id="setby" name="setby" placeholder="Name" 
                        @if (old('setby')!==NULL) value="{{ old('setby') }}" 
                        @elseif ($appointment->setby !== null) value="{{ $appointment->setby }}" 
                        @endif
                        {{--  @if ($appointment->status==true) readonly @endif  --}}
                        readonly>
                        <span class="help-block">
                          <strong id="setby-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div id="name_{{ $loop->index }}" class="form-group">
                        <label>Contact Name</label>
                        {{--  <p class="form-control">{{ $appointment->name }}</p>  --}}
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                        @if (old('name')!==NULL) value="{{ old('name') }}" 
                        @elseif ($appointment->name !== null) value="{{ $appointment->name }}" 
                        @endif
                        {{--  @if ($appointment->status==true) readonly @endif  --}}
                        >
                        <span class="help-block">
                          <strong id="name-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div id="address_{{ $loop->index }}" class="form-group">
                        <label>Contact Address</label>
                        {{--  <p class="form-control">{{ $appointment->address }}</p>  --}}
                        <input type="text" class="form-control" id="address" name="address" placeholder="Address"
                        @if (old('address')!==NULL) value="{{ old('address') }}" 
                        @elseif ($appointment->address !== null) value="{{ $appointment->address }}" 
                        @endif
                        {{--  @if ($appointment->status==true) readonly @endif  --}}
                        >
                        <span class="help-block">
                          <strong id="address-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div id="city_{{ $loop->index }}" class="form-group">
                        <label>Contact City</label>
                        {{--  <p class="form-control">{{ $appointment->city }}</p>  --}}
                        <input type="text" class="form-control" id="city" name="city" placeholder="City"
                        @if (old('city')!==NULL) value="{{ old('city') }}" 
                        @elseif ($appointment->city !== null) value="{{ $appointment->city }}" 
                        @endif
                        {{--  @if ($appointment->status==true) readonly @endif  --}}
                        >
                        <span class="help-block">
                          <strong id="city-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div id="state_{{ $loop->index }}" class="form-group">
                        <label>Contact State</label>
                        {{--  <p class="form-control">{{ $appointment->state }}</p>  --}}
                        <select class="form-control" id="state" name="state" required>
                          @foreach ($states as $state)
                          <option value="{{ $state->id }}"
                            @if (old('state') == $state->id) selected 
                            @elseif ($appointment['state'] == $state->name) selected 
                            @endif>{{ $state->name }}</option>
                          @endforeach
                        </select>
                        <span class="help-block">
                          <strong id="state-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div id="zip_code_{{ $loop->index }}" class="form-group">
                        <label>Contact Zip code</label>
                        {{--  <p class="form-control">{{ $appointment->zip_code }}</p>  --}}
                        <input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Zip code"
                        @if (old('zip_code')!==NULL) value="{{ old('zip_code') }}" 
                        @elseif ($appointment->zip_code !== null) value="{{ $appointment->zip_code }}" 
                        @endif
                        {{--  @if ($appointment->status==true) readonly @endif  --}}
                        >
                        <span class="help-block">
                          <strong id="zip-code-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div id="phone_number_{{ $loop->index }}" class="form-group">
                        <label>Contact Phone number</label>
                        {{--  <p class="form-control">{{ $appointment->phone_number }}</p>  --}}
                        <input type="tel" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number"
                        @if (old('phone_number')!==NULL) value="{{ old('phone_number') }}" 
                        @elseif ($appointment->phone_number !== null) value="{{ $appointment->phone_number }}" 
                        @endif
                        {{--  @if ($appointment->status==true) readonly @endif  --}}
                        >
                        <span class="help-block">
                          <strong id="phone-number-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div id="email_{{ $loop->index }}" class="form-group">
                        <label>Contact Email</label>
                        {{--  <p class="form-control">{{ $appointment->email }}</p>  --}}
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                        @if (old('email')!==NULL) value="{{ old('email') }}" 
                        @elseif ($appointment->email !== null) value="{{ $appointment->email }}" 
                        @endif
                        {{--  @if ($appointment->status==true) readonly @endif  --}}
                        >
                        <span class="help-block">
                          <strong id="email-{{ $loop->index }}"></strong>
                        </span>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-action" onClick="updateAppointment({{ $loop->index }})">@if ($appointment->status) Update @else Approve @endif</button>
                </div>                
                <button type="button" class="btn btn-action" data-toggle="collapse" data-target="#detail{{ $loop->index }}" style="margin-right: 16px;">More Details</button>
                <button type="button" class="btn btn-action appointment-remove">Remove</button>
              </form>
            </div>
            @endif
          </div>
          @empty
          @endforelse
          <div class="panel panel-warning">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOpen" aria-expanded="false"
                  aria-controls="collapseOpen">
                  Open
                </a>
              </h4>
            </div>
            <div id="collapseOpen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body" id="open_time">
                <input type="hidden" id="timestamp" name="timestamp" value="{{ $title->getTimestamp() }}" >
                <div class="input-group">
                  <div class="col-xs-6">
                    <div class="row">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control start_time" id="start_time" placeholder="08:00 AM" name="start_time">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="row">
                      <div class="input-group">
                        <span class="input-group-addon">~</span>
                        <input type="text" class="form-control end_time" id="end_time" placeholder="10:00 AM" name="end_time">
                      </div>
                    </div>
                  </div>
                </div>
                <div style="margin: 16px 0px;">
                  <button type="button" class="btn btn-action text-purple" id="available_time" style="margin-right: 16px;">Make Time Available</button>
                  <button type="button" class="btn btn-action text-red" id="unavailable_time">Make Time Unavailable</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop

@section('js')
<script>
    var currentTimestamp = "{{ $timeStamp }}";
    let clients = @json($clients);
    let advisor_agency = "{{ Auth::user()->company }}"
</script>
<script src="{{ asset('js/schedule.js') }}"></script>

@stop