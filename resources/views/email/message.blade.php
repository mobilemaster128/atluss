<pre style="font-size: 14px;">{{ $data}}</pre>
<p><a href={{$link}}> {{$name}}'s Schedule</a></p>
<p>Please do not respond directly to this email, this inbox is not monitored.</p>
