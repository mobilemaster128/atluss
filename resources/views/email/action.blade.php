<div style="display: block; width: 100%;">
    <a href="{{$url}}" style="padding: 16px; display: inline-block; text-decoration: none; min-width: 128px; background: #5290b9; border-radius: 4px; color: white; font-size: 16px;">{{$title}}</a>
</div>