@extends('template')

@section('title_postfix', 'Feedback')

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      @include('partials.contactus')
    </div>
  </div>
</section>
@stop 