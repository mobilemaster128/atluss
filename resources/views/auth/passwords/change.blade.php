@extends('basic.page') 
@section('title_postfix', 'Reset') 
@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Welcome</div>
</div>
@stop 
@section('banner')
<div class="container">
</div>
@stop 
@section('content')
<section class="section-atluss">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="pricing-frame">
                    <form action="{{ url('password/change') }}" method="post" id="change-form" class="login-form">
                        {!! csrf_field() !!}
                        <div class="heading text-center">Change Password</div>
                        <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Current password" name="current_password" id="current_password" value="{{ $email or old('email') }}" required>
                            @if ($errors->has('current_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Confirm password" name="password_confirmation" id="password_confirmation" required>
                            @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-black round btn-login">Change Password >></button>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('feature') 
@stop