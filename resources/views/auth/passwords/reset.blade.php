@extends('basic.page') 
@section('title_postfix', 'Reset') 
@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Welcome</div>
</div>
@stop 
@section('banner')
<div class="container">
</div>
@stop 
@section('content')
<section class="section-atluss">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="pricing-frame">
                    <form action="{{ url('password/reset') }}" method="post" id="reset-form" class="login-form">
                        {!! csrf_field() !!}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="heading text-center">Reset Password</div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ $email or old('email') }}" required>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Confirm password" name="password_confirmation" id="password_confirmation" required>
                            @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-black round btn-login">Reset Password >></button>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('feature') 
@stop