@extends('basic.page') 
@section('title_postfix', 'Login') 
@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Welcome</div>
</div>
@stop 
@section('banner')
<div class="container">
</div>
@stop 
@section('content')
<section class="section-atluss">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="pricing-frame">
                    <form @isset($invite) action="{{ url('login/accept') }}" @endisset
                        @empty($invite) action="{{ url('login') }}" @endempty
                        method="post" id="login-form" class="login-form">
                        <input type="hidden" id="timezone" name="timezone">
                        {!! csrf_field() !!}
                        @if (session('confirmation-danger'))
                            <div class="alert alert-danger">
                                {!! session('confirmation-danger') !!}
                            </div>
                        @else
                            @isset ($danger)
                                <div class="alert alert-success">
                                    {{ $danger }}
                                </div>
                            @endisset
                            @empty ($danger)
                                @if (session('confirmation-success'))
                                    <div class="alert alert-success">
                                        {{ session('confirmation-success') }}
                                    </div>
                                @else
                                    @isset ($success)
                                        <div class="alert alert-success">
                                            {{ $success }}
                                        </div>
                                    @endisset
                                @endif
                            @endempty
                        @endif
                        <div class="heading text-center">Login</div>
                        @isset($invite)
                        <input type="hidden" id="invite_id" name="invite_id" value="{{ $invite->id }}" >
                        @endisset
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email"
                            @isset($invite) value="{{ $invite->email }}" readonly @endisset
                            @empty($invite) value="{{ old('email') }}" @endempty required>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" id="remember"> Remember Me
                            </label>
                        </div>
                        <div class="form-group text-right">
                            <a href="{{ url('password/reset') }}" class="f_pswd">Forgot Password</a>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-black round btn-login">Login >></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('feature') 
@stop

@section('js')
<script>
  let timeoffset = new Date().getTimezoneOffset();
  $('#timezone').val(timeoffset)
</script>
@stop