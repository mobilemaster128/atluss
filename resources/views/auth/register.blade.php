@extends('basic.page') 
@section('title_postfix', 'Register') 
@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Welcome</div>
</div>
@stop 
@section('banner')
<div class="container">
</div>
@stop 
@section('content')
<section class="section-atluss">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="pricing-frame">
                    <form @isset($invite) action="{{ url('register/accept') }}" @endisset
                           @empty($invite) action="{{ url('register') }}" @endempty
                           method="post" id="register-form" class="login-form">
                        {!! csrf_field() !!}
                        @if (session('confirmation-success'))
                            <div class="alert alert-success">
                                {{ session('confirmation-success') }}
                            </div>
                        @else
                            @isset ($success)
                                <div class="alert alert-success">
                                    {{ $success }}
                                </div>
                            @endisset
                        @endif
                        <div class="heading text-center">Signup</div>
                        @isset($invite)
                        <input type="hidden" id="invite_id" name="invite_id" value="{{ $invite->id }}" >
                        @endisset
                        <div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Company Name" id="company" name="company"                            
                            @isset($invite->agency_id) value="{{ $invite->agency->name }}" @endisset
                            @empty($invite) value="{{ old('company') }}" @endempty
                            @isset($invite->agency_id) readonly @endisset required>
                            @if ($errors->has('company'))
                            <span class="help-block">
                                <strong>{{ $errors->first('company') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Full Name" id="name" name="name" value="{{ old('name') }}" required>
                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" placeholder="Email" id="email" name="email"
                                   @isset($invite) value="{{ $invite->email }}" readonly @endisset
                                   @empty($invite) value="{{ old('email') }}" @endempty
                                   required>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Retype Password" id="password_confirmation" name="password_confirmation" required>
                            @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                            {!! Recaptcha::render([ 'lang' => 'en' ]) !!}
                            @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-grou text-center">
                            <button type="submit" class="btn btn-black round btn-login">Sign up >></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('feature') 
@stop