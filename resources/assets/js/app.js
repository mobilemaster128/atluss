
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// custom js
require('bootstrap-timepicker');
require('chart.js');
require('es6-promise').polyfill();
require('jquery-editable-select');
require('jquery-popup-overlay');
require('jquery-mask-plugin');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// Vue.component("graph-component", require("./components/Graph.vue"));
// Vue.component("statistic-component", require("./components/StatisticComponent.vue"));

// const app = new Vue({
//     el: '.wrapper'
// });

window.layer = require('layui-layer');

// require("./graphs");