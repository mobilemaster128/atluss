<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'invalid_appointment' => 'Invalid Appointment',
    'invalid_time' => 'Invalid Time',
    'invalid_timeslot' => 'Invalid Timeslot',
    'invalid_advisor' => 'Invalid Advisor',
    'invalid_agency' => 'Invalid Agency Name',

];
