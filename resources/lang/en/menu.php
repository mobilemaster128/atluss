<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'account_settings' => 'Account Setting',
    'change_password' => 'Change Password',
    'dashboard' => 'Dashboard',
    'user_manage' => 'User List',
    'user_list' => 'User List',
    'invite_user' => 'Invite User',
    'invite_manage' => 'Invite Manage',

];
