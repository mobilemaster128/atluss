<?php

return [
    'email-title' => 'Congratulations on joining the Atluss family! Our Virtual Connection Toolkit is sure to keep you working at your peak productivity.',
    'email-intro'=> 'Please click HERE to verify that you’re ready to begin!',
    'email-button' => 'Email verification',
    'message' => 'Thanks for signing up! Please check your email.',
    'success' => 'You have successfully verified your account! You can now login.',
    'again' => 'You must verify your email before you can access the site. ' .
                '<br>If you have not received the confirmation email check your spam folder.'.
                '<br>To get a new confirmation email please <a href="' . url('confirmation/resend') . '" class="alert-link">click here</a>.',
    'resend' => 'A confirmation message has been sent. Please check your email.'
];
